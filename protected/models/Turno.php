<?php

Yii::import('application.models._base.BaseTurno');

class Turno extends BaseTurno
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public static function getTurnoActual(){
            $criteria = new CDbCriteria;
            $criteria->condition='estado="ACTIVO"';
            $criteria->limit=1;

            $model_turno = Turno::model()->findByAttributes(array(),$criteria);
            if(isset($model_turno)){
                $turno_actual['cajero_id']=$model_turno->cajero_id;
                $turno_actual['turno_id']=$model_turno->id;
                return $turno_actual;
            }else{
                $turno_actual['cajero_id']=1;
                $turno_actual['turno_id']=1;
                return $turno_actual;
            }
                
            
        }
}