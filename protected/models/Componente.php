<?php

Yii::import('application.models._base.BaseComponente');

class Componente extends BaseComponente
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'cantidad' => Yii::t('app', 'Cantidad'),
			'formula_id' => null,
			'producto_id' => null,
			'formula' => null,
			'producto' => null,
		);
	}
}