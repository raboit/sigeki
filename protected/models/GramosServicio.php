<?php

Yii::import('application.models._base.BaseGramosServicio');

class GramosServicio extends BaseGramosServicio
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function primaryKey(){
            return 'fecha_servicio';
        }
}