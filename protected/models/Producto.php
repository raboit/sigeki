<?php

Yii::import('application.models._base.BaseProducto');

class Producto extends BaseProducto
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function behaviors(){
            return array(
                'ProductoBehavior'=>array('class'=>'application.behaviors.ProductoBehavior')
                );
        } 
        public function rules() {
            
            return array_merge(array(
                array('nombre, tipo','filter','filter'=>'strtoupper'),
                array('tipo','in','range'=>array('TERMINADO','PROCESO'),'allowEmpty'=>false),
            ),parent::rules());
	}
        
        public function scopes()
        {
            return array(
                'proceso'=>array(
                    'condition'=>'estado="PROCESO"',
                ),
                'terminado'=>array(
                    'condition'=>'estado="TERMINADO"',
                ),
            );
        }
      
}