<?php

/**
 * This is the model base class for the table "componente".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Componente".
 *
 * Columns in table "componente" available as properties of the model,
 * followed by relations of table "componente" available as properties of the model.
 *
 * @property integer $id
 * @property integer $cantidad_gramo
 * @property integer $formula_id
 * @property integer $producto_id
 *
 * @property Formula $formula
 * @property Producto $producto
 */
  
abstract class BaseComponente extends GxActiveRecord {

       
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'componente';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Componente|Componentes', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('formula_id, producto_id', 'required'),
			array('cantidad_gramo, formula_id, producto_id', 'numerical', 'integerOnly'=>true),
			array('cantidad_gramo', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, cantidad_gramo, formula_id, producto_id', 'safe', 'on'=>'search'),
		);                        
	}

	public function relations() {
		return array(
			'formula' => array(self::BELONGS_TO, 'Formula', 'formula_id'),
			'producto' => array(self::BELONGS_TO, 'Producto', 'producto_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'cantidad_gramo' => Yii::t('app', 'Cantidad Gramo'),
			'formula_id' => null,
			'producto_id' => null,
			'formula' => null,
			'producto' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('cantidad_gramo', $this->cantidad_gramo);
		$criteria->compare('formula_id', $this->formula_id);
		$criteria->compare('producto_id', $this->producto_id);
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        
  
        
}