<?php

Yii::import('application.models._base.BaseDetalleProducto');

class DetalleProducto extends BaseDetalleProducto
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function behaviors(){
            return array(
                'DetalleProductoBehavior'=>array('class'=>'application.behaviors.DetalleProductoBehavior')
                );
        } 
        
        public static function getColumns(){
            return array(
            //'id',
                array(
                    'name'=>'producto_id',
                    'value'=>'GxHtml::valueEx($data->producto)',
                    'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
                ),
		array('name'=>'codigo_barra','header'=>'Codigo de barra'),
                array('name'=>'fecha_creacion','header'=>'Fecha de creación'),
                array('name'=>'fecha_modificacion','header'=>'Fecha de modificación'),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>'$data->getLabel()','filter'=>array('VENDIDO'=>'VENDIDO','NUEVO'=>'NUEVO','PROCESO'=>'PROCESO')),
                
                
                array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/borrar", array("id"=>$data->id))',
          ),
	);
        }
        
        public static function getColumnsCompra(){
            return array(
//            'id',
//                array(
//                    'name'=>'producto_id',
//                    'value'=>'GxHtml::valueEx($data->producto)',
//                    'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
//                ),
		array('name'=>'codigo_barra','header'=>'Codigo de barra'),
                array('name'=>'fecha_creacion','header'=>'Fecha de creación'),
                array('name'=>'fecha_modificacion','header'=>'Fecha de modificación'),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>'$data->getLabel()','filter'=>array('VENDIDO'=>'VENDIDO','NUEVO'=>'NUEVO','PROCESO'=>'PROCESO')),
                array('name'=>'total','header'=>'Total','value'=>'$data->producto->precio_venta'),
                
                array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/detalleProducto/borrar", array("id"=>$data->id))',
          ),
	);
        }
        
        public function scopes()
        {
            return array(
                'vendidos'=>array(
                    'condition'=>'estado="VENDIDO"',
                ),
                'stock'=>array(
                    'condition'=>'estado="NUEVO"',
                ),
                'procesados'=>array(
                    'condition'=>'estado="PROCESO"',
                ),
            );
        }
}