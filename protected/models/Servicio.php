<?php

Yii::import('application.models._base.BaseServicio');

class Servicio extends BaseServicio
{
        public $precio;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function behaviors(){
            return array(
                'ServicioBehavior'=>array('class'=>'application.behaviors.ServicioBehavior')
                );
        }
        
        public static function getColumns(){
            return array(
		'fecha',
                array(
                                'name'=>'tipo_servicio_id',
                                'value'=>'GxHtml::valueEx($data->tipoServicio)',
                                'filter'=>GxHtml::listDataEx(TipoServicio::model()->findAllAttributes(null, true)),
                                ),
                array(
                                'name'=>'cliente_id',
                                'value'=>'GxHtml::valueEx($data->cliente)',
                                'filter'=>GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true)),
                                ),
		array(
				'name'=>'estilista_id',
				'value'=>'GxHtml::valueEx($data->estilista)',
				'filter'=>GxHtml::listDataEx(Estilista::model()->findAllAttributes(null, true)),
				),
		
		
		array(
				'name'=>'formula_id',
				'value'=>'GxHtml::valueEx($data->formula)',
				'filter'=>GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true)),
				),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>'$data->getLabel()','filter'=>array('NUEVO'=>'NUEVO','FINALIZADO'=>'FINALIZADO')),
                array('type'=>'raw','name'=>'precio_venta','header'=>'Precio Venta','value'=>'$data->tipoServicio->precio_venta','filter'=>false),
                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'viewButtonUrl'=>'Yii::app()->controller->createUrl("ver", array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->controller->createUrl("actualizar", array("id"=>$data->id))',
                        'deleteButtonUrl'=>'Yii::app()->controller->createUrl("borrar", array("id"=>$data->id))',
                  ),       
                
                
                
            );
        }
        
        public function scopes()
        {
            return array(
                'finalizados'=>array(
                    'order'=>'id DESC',
                    'condition'=>'estado="FINALIZADO"',
                ),
            );
        }
        
        public function actualizarStock()
        {
            if(isset($this->formula)){
            foreach ($this->formula->componentes as $componente) {
                $componente->producto->cantidad_gramos = $componente->producto->cantidad_gramos - $componente->cantidad_gramo;
                $componente->producto->save(true);
            }
            
        }}
}
