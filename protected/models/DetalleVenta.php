<?php

Yii::import('application.models._base.BaseDetalleVenta');

class DetalleVenta extends BaseDetalleVenta
{
    
        public function behaviors(){
            return array(
                'DetalleVentaBehavior'=>array('class'=>'application.behaviors.DetalleVentaBehavior')
                );
        }
    
        public static function getColumnsCrearDetalleVenta(){
            return array(
            'id',
                array('name'=>'tipodetalle','value'=>'$data->getTipoDetalle()','header'=>'Tipo detalle',),
                array('name'=>'nombredetalle','value'=>'$data->getNombreDetalle()','header'=>'Nombre',),
                'precio',
		array('type'=>'raw','name'=>'no_aplicable','header'=>'Aplicable','value'=>'($data->no_aplicable) ? TbHtml::labelTb("No aplicable", array("color" => TbHtml::LABEL_COLOR_INFO)) : TbHtml::labelTb("Aplicable", array("color" => TbHtml::LABEL_COLOR_SUCCESS))'),
                
		
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/detalleVenta/borrar", array("id"=>$data->id))',
                'template'=>'{delete}'
          
            ),
            
	);
        }
        public static function getColumnsVerVenta(){
            return array(
                         array(
                            'name'=>'tipodetalle',
                            'value'=>'$data->getTipoDetalle()',
                            'header'=>'Tipo detalle',
                            ),
                        array(
                            'name'=>'nombredetalle',
                            'value'=>'$data->getNombreDetalle()',
                            'header'=>'Nombre',
                            ),
                        'precio',
                        array('type'=>'raw','name'=>'no_aplicable','header'=>'Aplicable','value'=>'($data->no_aplicable) ? TbHtml::labelTb("No aplicable", array("color" => TbHtml::LABEL_COLOR_INFO)) : TbHtml::labelTb("Aplicable", array("color" => TbHtml::LABEL_COLOR_SUCCESS))'), 
                        array(
                            'name'=>'Estilista',
                            'type'=>'raw',
                            'value'=>'$data->getAsignacion()',
                        ),



                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>(Yii::app()->user->getIsSuperuser())?'{view}{update}{delete}':'{delete}',
                        'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/detalleVenta/borrar", array("id"=>$data->id))',
                        'viewButtonUrl'=>'Yii::app()->controller->createUrl("/detalleVenta/ver", array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->controller->createUrl("/detalleVenta/actualizar", array("id"=>$data->id))',
                    
                        
                        
                  ),                
                );
        }
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
             
        public function scopes()
        {
            return array(
                'servicios'=>array(
                    'condition'=>'producto_id is NULL AND servicio_id is not NULL AND servicio.estado!="N/A"',
                ),
                'servicios_no_asignados'=>array(
                    'condition'=>'producto_id is NULL AND servicio_id is not NULL AND servicio.estado="NUEVO" AND no_aplicable IS NULL',
                    'with'=>array('servicio'),
                ),
                'productos'=>array(
                    'condition'=>'servicio_id is NULL AND producto_id is not NULL',
                ),
            );
        }
}
