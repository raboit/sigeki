<?php

Yii::import('application.models._base.BaseVenta');

class Venta extends BaseVenta
{
        public $total_tipo_pago;
        
        public function behaviors(){
            return array(
                'VentaBehavior'=>array('class'=>'application.behaviors.VentaBehavior')
                );
        }        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('total', $this->total);
		$criteria->compare('cajero_id', $this->cajero_id);
		$criteria->compare('turno_id', $this->turno_id);
                $criteria->compare('estado', $this->estado, true);
		$criteria->compare('cliente_id', $this->cliente_id);
                
                $criteria->order = "id  DESC";
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        public static function getColumns(){
            return array(
                array('name'=>'id','htmlOptions'=>array('width'=>'15px')),
		array(
                    'name'=>'cliente_id',
                    'header'=>'Cliente',
                    'value'=>'GxHtml::valueEx($data->cliente)',
                    'filter'=>GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true)),
                ),
                array(
                    'name'=>'cajero_id',
                    'header'=>'Cajero',
                    'value'=>'GxHtml::valueEx($data->cajero)',
                    'filter'=>GxHtml::listDataEx(Cajero::model()->findAllAttributes(null, true)),
                ),
                
                array('name'=>'fecha','header'=>'Fecha',),
                array('name'=>'tipo_pago','header'=>'Tipo de pago',),
                array('type'=>'raw','name'=>'total','header'=>'Total','value'=>'$data->calcularTotal()','htmlOptions'=>array('width'=>'100px',)),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>'$data->getLabel()','filter'=>array('NUEVA'=>'NUEVA','PAGADA'=>'PAGADA','ANULADA'=>'ANULADA')),
                
		
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("/venta/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("/venta/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/venta/borrar", array("id"=>$data->id))',
                'buttons'=>array(
                        'delete'=>array(
                            'visible'=>'(Yii::app()->user->getIsSuperuser())?true:false',
                        ),
                        'update'=>array(
                            'visible'=>'(Yii::app()->user->getIsSuperuser())?true:false',
                        ),
                    ),
          ),                
	);
        }
        
        public static function getColumnsArqueo(){
            return array(
                //array('name'=>'id','htmlOptions'=>array('width'=>'15px')),
//                array(
//                    'name'=>'cajero_id',
//                    'header'=>'Cajero',
//                    'value'=>'GxHtml::valueEx($data->cajero)',
//                    'filter'=>GxHtml::listDataEx(Cajero::model()->findAllAttributes(null, true)),
//                ),
//                
//                array('name'=>'fecha','header'=>'Fecha',),
                array('name'=>'tipo_pago','header'=>'Tipo de pago',),
                array('name'=>'total','header'=>'Total','value'=>'$data->total_tipo_pago'),
                
                //array('type'=>'raw','name'=>'total','header'=>'Total','value'=>'$data->calcularTotal()','htmlOptions'=>array('width'=>'100px',)),
                //array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>'$data->getLabel()','filter'=>array('NUEVA'=>'NUEVA','PAGADA'=>'PAGADA','ANULADA'=>'ANULADA')),
                
		
                      
	);
        }
        
        public function scopes()
        {
            return array(
                'arqueo_caja'=>array(
                    'order'=>'tipo_pago DESC',
                    'select'=>'*, SUM(total) AS total_tipo_pago',
                    'group'=>'tipo_pago',
                    //'condition'=>'estado="VENDIDO"',
                ),
                'pagadas'=>array(
                    'order'=>'id DESC',
                    'condition'=>'estado="PAGADA"',
                ),
            );
        }
        
        
        
        
}
