<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('view', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('codigo_barra')); ?>:
        <?php echo GxHtml::encode($data->codigo_barra); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_creacion')); ?>:
        <?php echo GxHtml::encode($data->fecha_creacion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
        <?php echo GxHtml::encode($data->estado); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_modificacion')); ?>:
        <?php echo GxHtml::encode($data->fecha_modificacion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('producto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->producto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('compra_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->compra)); ?>
	<br />

</div>
</a>