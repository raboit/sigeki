<?php

$this->breadcrumbs = array(
	$model->label(2) => array('administrar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'nombre',
		'celular',
		'direccion',
                'email',
		'fecha_nacimiento',
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => array(
        array('label' => 'Cartilla', 'content' => $this->renderPartial('cartillas/administrar', array('model' => new CArrayDataProvider($model->formulas,array()),'buttons' => 'create'),true,false), 'active' => true),
        array('label' => 'Servicios', 'content' => $this->renderPartial('servicios/administrar', array('model' => new CArrayDataProvider($model->servicios(array('scopes'=>array('finalizados'))),array()),'buttons' => 'create'),true,false)),
        array('label' => 'Ventas', 'content' => $this->renderPartial('ventas/administrar', array('model' => new CArrayDataProvider($model->ventas(array('scopes'=>array('pagadas'))),array()),'buttons' => 'create'),true,false)),
    ),
)); ?>

