
<?php $this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'venta-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}\n{extendedSummary}",
        //'htmlOptions' => array(
        //                        'style' => 'overflow-y:auto;'
        //                                   .'table-layout:fixed;'
        //                                   .'white-space:nowrap;'
        //                                   ),       
	'columns' => array(
		
		'fecha',
		//'total',
		array(
				'name'=>'Cajero',
				'value'=>'GxHtml::valueEx($data->cajero)',
				'filter'=>GxHtml::listDataEx(Cajero::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'Total',
				'value'=>'$data->calcularTotal()',
				//'filter'=>GxHtml::listDataEx(Turno::model()->findAllAttributes(null, true)),
				),
            
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("venta/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("venta/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("venta/borrar", array("id"=>$data->id))',
          ),                
	),
    
        'extendedSummary' => array(
	'title' => 'Total',
	'columns' => array(
	'Total' => array(
		'label'=>'Total',
		'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),)),
            
        'extendedSummaryOptions' => array(
	'class' => 'well pull-right',
	'style' => 'width:300px'
	),
)); ?>
