<?php $label_model = Servicio::model()->attributeLabels(); ?>
<?php $this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'servicio-grid',
	'dataProvider' => $model,
	//'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}\n{extendedSummary}",
        //'htmlOptions' => array(
        //                        'style' => 'overflow-y:auto;'
        //                                   .'table-layout:fixed;'
        //                                   .'white-space:nowrap;'
        //                                   ),       
	'columns' => array(
		'id',
                array(
                    'name'=> $label_model['fecha'],
                    'value'=>'$data->fecha',
                                ),
                         	
		array(
				'name'=>'Estilista',
				'value'=>'GxHtml::valueEx($data->estilista)',
				'filter'=>GxHtml::listDataEx(Estilista::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'Tipo de Servicio',
				'value'=>'GxHtml::valueEx($data->tipoServicio)',
				'filter'=>GxHtml::listDataEx(TipoServicio::model()->findAllAttributes(null, true)),
				),
		
		array(
				'name'=>'Formula',
				'value'=>'GxHtml::valueEx($data->formula)',
				'filter'=>GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true)),
				),
            
                array(
				'name'=>'Total',
				'value'=>'$data->tipoServicio->precio_venta',
				'filter'=>GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true)),
				),
            
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("servicio/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("servicio/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("servicio/borrar", array("id"=>$data->id))',
          ),                
	),
    
        'extendedSummary' => array(
	'title' => 'Total',
	'columns' => array(
	'Total' => array(
		'label'=>'Total',
		'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),)),
            
        'extendedSummaryOptions' => array(
	'class' => 'well pull-right',
	'style' => 'width:300px'
	),
)); ?>

