<?php $label_model = Formula::model()->attributeLabels(); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'formula-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",  
	'columns' => array(
		'id',
                array(
                    'name'=> $label_model['nombre'],
                    'value'=>'$data->nombre',
                ),
		array(
                    'name'=> $label_model['fecha_creacion'],
                    'value'=>'$data->fecha_creacion',
                ),		
		array(
                    'name'=> $label_model['fecha_edicion'],
                    'value'=>'$data->fecha_edicion',
                ),       
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'viewButtonUrl'=>'Yii::app()->controller->createUrl("formula/ver", array("id"=>$data->id))',
                    'updateButtonUrl'=>'Yii::app()->controller->createUrl("formula/actualizar", array("id"=>$data->id))',
                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("formula/borrar", array("id"=>$data->id))',
                ),                
	),
)); ?>
