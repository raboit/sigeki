<?php

$this->breadcrumbs = array(
	$model->label(2) => array('list'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('list'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create'), 'url'=>array('create'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), TbHtml::labelTb('Admin')); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'fecha',
		array(
			'name' => 'estilista',
			'type' => 'raw',
			'value' => $model->estilista !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->estilista)), array('estilista/view', 'id' => GxActiveRecord::extractPkValue($model->estilista, true))) : null,
			),
		array(
			'name' => 'tipoServicio',
			'type' => 'raw',
			'value' => $model->tipoServicio !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->tipoServicio)), array('tipoServicio/view', 'id' => GxActiveRecord::extractPkValue($model->tipoServicio, true))) : null,
			),
		array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/view', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),
		array(
			'name' => 'formula',
			'type' => 'raw',
			'value' => $model->formula !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->formula)), array('formula/view', 'id' => GxActiveRecord::extractPkValue($model->formula, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('detalleVentas')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->detalleVentas as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('detalleVenta/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>