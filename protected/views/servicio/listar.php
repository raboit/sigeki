<?php

$this->breadcrumbs = array(
	Servicio::label(2),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . Servicio::label(), 'url' => array('venta/crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . Servicio::label(2), 'url' => array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
        array('label'=>Yii::t('app', 'Up'), 'url'=>'javascript:GoUp()', 'icon'=>'arrow-up', 'id'=>'button-up'),
);
?>
<?php echo TbHtml::pageHeader(GxHtml::encode(Servicio::label(2)), null); ?>


<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('inventario/inventarioDetalle');?>">
                <div class="view well cuadro">
                        <span>
                            <h4><?php echo "Servicios realizados" ?></h4>
                            <?php echo "Todos los servicios realizados en Kika's salon" ?>
                        </span>
                </div>
        </a>
</div>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('inventario/inventarioDetalle');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Tipos de servicios" ?></h4>

                        <?php echo "Tipos de servicios disponibles en Kika's salon" ?>
                         
                        </span>
                </div>
        </a>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/scroll.js'); ?>