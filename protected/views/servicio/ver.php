<?php

$this->breadcrumbs = array(
	$model->label(2) => array('administrar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'fecha',
		array(
			'name' => 'estilista',
			'type' => 'raw',
			'value' => $model->estilista !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->estilista)), array('estilista/ver', 'id' => GxActiveRecord::extractPkValue($model->estilista, true))) : null,
			),
		array(
			'name' => 'tipoServicio',
			'type' => 'raw',
			'value' => $model->tipoServicio !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->tipoServicio)), array('tipoServicio/ver', 'id' => GxActiveRecord::extractPkValue($model->tipoServicio, true))) : null,
			),
		array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),
		array(
			'name' => 'formula',
			'type' => 'raw',
			'value' => $model->formula !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->formula)), array('formula/ver', 'id' => GxActiveRecord::extractPkValue($model->formula, true))) : null,
			),
	),
)); ?>
