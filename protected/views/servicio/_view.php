<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('view', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
        <?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estilista_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->estilista)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_servicio_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->tipoServicio)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cliente)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('formula_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->formula)); ?>
	<br />

</div>
</a>