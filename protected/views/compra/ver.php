<?php

$this->breadcrumbs = array(
	$model->label(2) => array('administrar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'precio_compra',
		'fecha_compra',
                //'cantidad_productos',
		array(
			'name' => 'producto',
			'type' => 'raw',
			'value' => $model->producto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->producto)), array('producto/ver', 'id' => GxActiveRecord::extractPkValue($model->producto, true))) : null,
			),
		array(
			'name' => 'proveedor',
			'type' => 'raw',
			'value' => $model->proveedor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proveedor)), array('proveedor/ver', 'id' => GxActiveRecord::extractPkValue($model->proveedor, true))) : null,
			),
                'cantidad_productos'
                //array('type'=>'raw','name'=>'cantidad_productos','value'=>$model->calcularDetalles(),'htmlOptions'=>array('width'=>'100px',)),
	),
)); ?>

