<script type="text/javascript">
function agregarProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("proveedor/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#proveedor_div').html(data.div);
                    $('#proveedor_div').show();
                    $('#proveedor_div form').submit(agregarProveedor);
                }
                else
                {
                    $('#proveedor_div').html(data.div);
                    actualizarProveedor();
                    setTimeout(\"$('#myModal').modal('hide');\",500);
                    
                 }              
            }",
            ))?>;
    return false; 
 
} 

function actualizarProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('proveedor/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'proveedor_id'),
            ))?>;
    return false; 
 
} 
</script>

<script type="text/javascript">
function agregarProducto()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("producto/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#producto_div').html(data.div);
                    $('#producto_div').show();
                    $('#producto_div form').submit(agregarProducto);
                }
                else
                {
                    $('#producto_div').html(data.div);
                    actualizarProducto();
                    setTimeout(\"$('#myModal2').modal('hide');\",500);
                    
                 }              
            }",
            ))?>;
    return false; 
 
} 

function actualizarProducto()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('producto/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'producto_id'),
            ))?>;
    return false; 
 
} 
</script>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'compra-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => true,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

                <?php echo $form->textFieldControlGroup($model,'fecha_compra', array('span'=>3, 'class'=>'datepicker')); ?>
		<?php echo $form->textFieldControlGroup($model,'precio_compra', array('append'=>'$','span'=>3)); ?>
                <?php echo $form->textFieldControlGroup($model,'cantidad_productos', array('span'=>3)); ?>
		<?php echo $form->dropDownListControlGroup($model,'producto_id', GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)), array('class'=>'span3 selectpicker', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?>
		<?php echo $form->dropDownListControlGroup($model,'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true)), array('class'=>'span3 selectpicker', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?>

<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
    TbHtml::linkButton('Nuevo Producto', array(
                    'icon'=>'plus white',
                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                    'onclick'=>'js:agregarProducto()',
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal2',
                    #'url' => Yii::app()->createUrl('venta/crearDetalleVenta',array('id'=>$model->id)),
                    )),
    
    TbHtml::linkButton('Nuevo proveedor', array(
                    'icon'=>'plus white',
                    'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                    'onclick'=>'js:agregarProveedor()',
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    #'url' => Yii::app()->createUrl('venta/crearDetalleVenta',array('id'=>$model->id)),
                    )),
    
));
?>
<?php $this->endWidget(); ?>        
<?php $this->widget('ext.select2.ESelect2'); ?>
<?php $this->widget('ext.datepicker.Datepicker'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>


        
<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'myModal',
    'header' => 'Crear nuevo proveedor',
    'content' => '<div id="proveedor_div"></div>',
)); ?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'myModal2',
    'header' => 'Crear nuevo producto',
    'content' => '<div id="producto_div"></div>',
)); ?>