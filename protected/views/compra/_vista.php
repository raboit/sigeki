<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('ver', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('precio_compra')); ?>:
        <?php echo GxHtml::encode($data->precio_compra); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_compra')); ?>:
        <?php echo GxHtml::encode($data->fecha_compra); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('producto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->producto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proveedor)); ?>
	<br />

</div>
</a>