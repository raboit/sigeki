<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'fecha_inicio',
		'fecha_termino',
		array(
			'name' => 'cajero',
			'type' => 'raw',
			'value' => $model->cajero !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cajero)), array('cajero/ver', 'id' => GxActiveRecord::extractPkValue($model->cajero, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode('Arqueo de caja'); ?></h2>
<?php
	$this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'servicio-grid',
	'dataProvider' => new CArrayDataProvider($model->ventas(array( 'scopes'=>array( 'arqueo_caja' ) )),array()),
	//'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{extendedSummary}{items}{pager}{summary}", 
	'columns' => Venta::getColumnsArqueo(),
        'extendedSummary' => array(
            'title' => 'Total',
            'columns' => array(
            'total' => array(
                    'label'=>'Total',
                    'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),)),            
        'extendedSummaryOptions' => array(
            'class' => 'well pull-right',
            'style' => 'width:300px'
	),
)); ?>
