
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'venta-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
        //'htmlOptions' => array(
        //                        'style' => 'overflow-y:auto;'
        //                                   .'table-layout:fixed;'
        //                                   .'white-space:nowrap;'
        //                                   ),       
	'columns' => array(
		'id',
		array(
                    'name'=>'Fecha',
                    'value'=>'$data->fecha',
		),
                array(
                    'name'=>'Total',
                    'value'=>'Yii::app()->format->formatNumber($data->total)',
		),
		array(
				'name'=>'Turno',
				'value'=>'GxHtml::valueEx($data->turno)',
				'filter'=>GxHtml::listDataEx(Turno::model()->findAllAttributes(null, true)),
				),
		
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("venta/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("venta/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("venta/borrar", array("id"=>$data->id))',
          ),                
	),
)); ?>
