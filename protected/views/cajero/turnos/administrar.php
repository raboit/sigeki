
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'turno-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
        //'htmlOptions' => array(
        //                        'style' => 'overflow-y:auto;'
        //                                   .'table-layout:fixed;'
        //                                   .'white-space:nowrap;'
        //                                   ),       
	'columns' => array(
                'id',
		array(
                    'name'=>'Fecha Inicio',
                    'value'=>'$data->fecha_inicio',
		),
		array(
                    'name'=>'Fecha Termino',
                    'value'=>'$data->fecha_termino',
		),
                array(
                    'name'=>'Estado',
                    'value'=>'$data->estado',
		),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'viewButtonUrl'=>'Yii::app()->controller->createUrl("venta/ver", array("id"=>$data->id))',
                    'updateButtonUrl'=>'Yii::app()->controller->createUrl("venta/actualizar", array("id"=>$data->id))',
                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("venta/borrar", array("id"=>$data->id))',
                ),                
        ),
)); ?>
