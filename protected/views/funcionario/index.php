<?php

$this->breadcrumbs = array(
	'Inventario' => (Yii::app()->user->returnUrl)?'javascript:history.back()':$this->createUrl('inventario/index'),
	'Inicio',
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
//        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
//        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
//        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
//        
        array('label'=>Yii::t('app', 'Other|Others', 2)),
//        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'Funcionarios'), null); ?>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('estilista/administrar');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Estilistas" ?></h4>
                        <?php echo "Ver Estilistas"; ?> 

                        </span>
                </div>
        </a>
</div>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('cajero/administrar');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Cajeros" ?></h4>

                        <?php echo "Ver Cajeros" ?>
                         
                        </span>
                </div>
        </a>
</div>
