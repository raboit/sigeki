<?php

$this->breadcrumbs = array(
	$model->label(2) => array('/compra/administrar'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('/compra/administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()), null); ?>
<?php
$this->renderPartial('compra/_formulario', array(
		'model' => $model,
		'buttons' => 'create'));
?>