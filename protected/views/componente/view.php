<?php

$this->breadcrumbs = array(
	$model->label(2) => array('list'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('list'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create'), 'url'=>array('create'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), TbHtml::labelTb('Admin')); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'cantidad',
		array(
			'name' => 'formula',
			'type' => 'raw',
			'value' => $model->formula !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->formula)), array('formula/view', 'id' => GxActiveRecord::extractPkValue($model->formula, true))) : null,
			),
		array(
			'name' => 'producto',
			'type' => 'raw',
			'value' => $model->producto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->producto)), array('producto/view', 'id' => GxActiveRecord::extractPkValue($model->producto, true))) : null,
			),
	),
)); ?>

