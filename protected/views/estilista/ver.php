<?php

$this->breadcrumbs = array(
	$model->label(2) => array('administrar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'nombre',
                'porcentaje',
	),
)); ?>

<?php $this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'componente-grid',
	'dataProvider' => new CArrayDataProvider($model->servicios,array()),
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}\n{extendedSummary}",
	'columns' => array(
                'id',
                'fecha',
                array(
                    'name'=>'Tipo Servicio',
                    'value'=>'$data->tipoServicio',
                    'filter'=>GxHtml::listDataEx(TipoServicio::model()->findAllAttributes(null, true)),
                    ),
                'cliente',
                'formula',
                array(
                    'name'=>'Total',
                    'value'=>'$data->tipoServicio->precio',
                    'filter'=>GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true)),
                    ),
                array(
                    'name'=>'Total Asignado',
                    'value'=>'$data->tipoServicio->precio*$data->estilista->porcentaje',
                    'filter'=>GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true)),
                    ),
                              	
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("/servicio/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("/servicio/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/servicio/borrar", array("id"=>$data->id))',
          ),                
	),
    
        'extendedSummary' => array(
	'title' => 'Total',
	'columns' => array(
	'Total' => array(
		'label'=>'Total Servicios',
		'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),
        'Total Asignado' => array(
		'label'=>'Total Asignado',
		'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),
            )),
            
        'extendedSummaryOptions' => array(
	'class' => 'well pull-right',
	'style' => 'width:300px'
	),
)); ?>
