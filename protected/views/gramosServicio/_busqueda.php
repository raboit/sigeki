<div class="wide form">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'search-gramos-servicio-form',
	'action'=>Yii::app()->createUrl($this->route),
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
	'method'=>'get',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<?php echo $form->textFieldControlGroup($model,'nombre_tipo_servicio', array('span'=>3, 'maxlength'=>200)); ?>

	<?php echo $form->textFieldControlGroup($model,'fecha_servicio', array('span'=>3)); ?>

	<?php echo $form->textFieldControlGroup($model,'nombre_estilista', array('span'=>3, 'maxlength'=>200)); ?>

	<?php echo $form->textFieldControlGroup($model,'nombre_producto', array('span'=>3, 'maxlength'=>200)); ?>

	<?php echo $form->textFieldControlGroup($model,'cantidad_gr', array('span'=>3, 'maxlength'=>11)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton(Yii::t('app', 'Search'),  array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'icon'=>'white search'));?>
        <?php echo TbHtml::resetButton(Yii::t('app', 'Reset'), array('icon'=>'icon-remove-sign')); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php $this->widget('ext.select2.ESelect2'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>

