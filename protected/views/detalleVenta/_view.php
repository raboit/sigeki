<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('view', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad')); ?>:
        <?php echo GxHtml::encode($data->cantidad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('precio')); ?>:
        <?php echo GxHtml::encode($data->precio); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('no_aplicable')); ?>:
        <?php echo GxHtml::encode($data->no_aplicable); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('venta_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->venta)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('servicio_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->servicio)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('producto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->producto)); ?>
	<br />

</div>
</a>