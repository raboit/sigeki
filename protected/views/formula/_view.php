<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('view', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
        <?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_creacion')); ?>:
        <?php echo GxHtml::encode($data->fecha_creacion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_edicion')); ?>:
        <?php echo GxHtml::encode($data->fecha_edicion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cliente)); ?>
	<br />

</div>
</a>