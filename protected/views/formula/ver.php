<?php

$this->breadcrumbs = array(
	$model->label(2) => 'javascript:history.back()',
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Nuevo Componente'), 'url'=>array('componente/crear', 'id' => $model->id), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
        
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'nombre',
		'fecha_creacion',
		'fecha_edicion',
		array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'componente-grid',
	'dataProvider' => new CArrayDataProvider($model->componentes,array()),
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
                'id',
            	array(
                    'name'=>'Producto',
                    'value'=>'GxHtml::valueEx($data->producto)',
                    'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
		),
                array(
                    'name'=>'Cantidad (gr)',
                    'value'=>'$data->cantidad_gramo',
                    'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
		),
		 
                	
        array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("/componente/ver", array("id"=>$data->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("/componente/actualizar", array("id"=>$data->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("/componente/borrar", array("id"=>$data->id))',
          ),                
	),
)); ?>
