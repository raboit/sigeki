<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'proveedor-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => false,
));
?>

<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
</p>
        
                <?php echo $form->textFieldControlGroup($model,'nombre_comercial', array('span'=>3, 'maxlength'=>200)); ?>
		<?php echo $form->textFieldControlGroup($model,'telefono', array('span'=>3, 'maxlength'=>45)); ?>
		<?php echo $form->textFieldControlGroup($model,'direccion', array('span'=>3, 'maxlength'=>200)); ?>


<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
));
?>
<?php $this->endWidget(); ?>        
