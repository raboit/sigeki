<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>
<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
        'nullDisplay'=>TbHtml::labelTb(Yii::t('app','N/A'), array('color' => TbHtml::LABEL_COLOR_IMPORTANT)),
	'attributes' => array(
		'id',
		'nombre',
		'cantidad_gramos',
		'cantidad_unidades',
		'precio_venta',
		'codigo_barra',
		'tipo',
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => array(
        array('label' => 'Compras', 'content' => $this->renderPartial('compras/administrar', array('model' => new CArrayDataProvider($model->compras,array()),'buttons' => 'create'),true,false), 'active' => true),
        ($model->tipo=="TERMINADO") ? array('label' => 'Ventas', 'content' => $this->renderPartial('detalles/administrar', array('model' => new CArrayDataProvider($model->detalleVentas,array())),true,false)): array(),
        //array('label' => 'Procesadas', 'content' => $this->renderPartial('detalles/administrar', array('model' => new CArrayDataProvider($model->detalleProductos(array('scopes'=>array('procesados'))),array()),'buttons' => 'create'),true,false)),
    ),
)); ?>