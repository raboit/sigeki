<?php $label_model = Producto::model()->attributeLabels(); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'detalle-venta-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",      
	'columns' => array(
		array(
                    'name'=>'precio',
                    'header'=>'Precio',
                ),
		array(
                    'name'=>'venta_id',
                    'value'=>'$data->venta->fecha',
                    'header'=>'Venta',
                    'filter'=>GxHtml::listDataEx(Venta::model()->findAllAttributes(null, true)),
                ),
		
		array(
                    'name'=>'producto_id',
                    'header'=>'Producto',
                    'value'=>'GxHtml::valueEx($data->producto)',
                    'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
                ),
            
            array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template'=>'{view}',
                    'viewButtonUrl'=>'Yii::app()->controller->createUrl("detalleVenta/ver", array("id"=>$data->id))',
            ),
		               
	),
)); ?>
