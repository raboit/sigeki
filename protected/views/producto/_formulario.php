<?php 
Yii::app()->clientScript->registerScript('tipo_producto','
    function tipo_producto(value){
        if(value=="TERMINADO"){
            $("#cantidad_gramos").hide();
            $("#cantidad_unidades").show();
            $("#precio_venta").show();
        }else{
            $("#cantidad_gramos").show();
            $("#cantidad_unidades").hide();
            $("#precio_venta").hide();
        }
    }
',CClientScript::POS_END);

Yii::app()->clientScript->registerScript('ready','
    $(function () {
    if($("#producto-form input[name=\'Producto[tipo]\']:checked").val()=="TERMINADO"){
        $("#cantidad_gramos").hide();
        $("#cantidad_unidades").show();
    }else{
        $("#cantidad_gramos").show();
        $("#cantidad_unidades").hide();
    }
  });
    
',CClientScript::POS_READY); 
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'producto-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => true,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldControlGroup($model,'nombre', array('onKeyup'=>'this.value=this.value.toUpperCase();','span'=>3, 'maxlength'=>200)); ?>
                <?php echo $form->radioButtonListControlGroup($model,'tipo',array('TERMINADO'=>'TERMINADO','PROCESO'=>'PROCESO'), array('id'=>'tipo','onclick'=>'tipo_producto(this.value)','span'=>3, 'maxlength'=>45)); ?>
		<div id="cantidad_gramos"><?php echo $form->textFieldControlGroup($model,'cantidad_gramos', array('span'=>3, 'append' => 'gr', 'maxlength'=>11)); ?></div>
		<div id="cantidad_unidades"><?php echo $form->textFieldControlGroup($model,'cantidad_unidades', array('span'=>3,'append' => 'un', 'maxlength'=>11)); ?></div>
		<div id="precio_venta"><?php echo $form->textFieldControlGroup($model,'precio_venta', array('span'=>3,'append' => '$', 'maxlength'=>11)); ?></div>
		<?php echo $form->textFieldControlGroup($model,'codigo_barra', array('span'=>3, 'maxlength'=>11)); ?>

<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
));
?>
<?php $this->endWidget(); ?>        
<?php $this->widget('ext.select2.ESelect2'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>
