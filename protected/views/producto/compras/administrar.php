
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'compras-grid',
	'dataProvider' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{items}{pager}",  
	'columns' => array(
		'id',
                array(
                    'name'=>'Precio Compra',
                    'value'=>'$data->precio_compra',
                    'filter'=>GxHtml::listDataEx(Compra::model()->findAllAttributes(null, true)),
                ),
		'cantidad_productos',
		array(
                    'name'=>'Fecha Compra',
                    'value'=>'$data->fecha_compra',
                    'filter'=>GxHtml::listDataEx(Compra::model()->findAllAttributes(null, true)),
                ),       
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template'=>'{view}',
                    'viewButtonUrl'=>'Yii::app()->controller->createUrl("compra/ver", array("id"=>$data->id))',
                    //'updateButtonUrl'=>'Yii::app()->controller->createUrl("compra/actualizar", array("id"=>$data->id))',
                    //'deleteButtonUrl'=>'Yii::app()->controller->createUrl("compra/borrar", array("id"=>$data->id))',
                ),                
	),
)); ?>
