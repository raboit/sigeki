<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('ver', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
        <?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad_gramos')); ?>:
        <?php echo GxHtml::encode($data->cantidad_gramos); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad_unidades')); ?>:
        <?php echo GxHtml::encode($data->cantidad_unidades); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('precio_venta')); ?>:
        <?php echo GxHtml::encode($data->precio_venta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('codigo_barra')); ?>:
        <?php echo GxHtml::encode($data->codigo_barra); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo')); ?>:
        <?php echo GxHtml::encode($data->tipo); ?>
	<br />

</div>
</a>