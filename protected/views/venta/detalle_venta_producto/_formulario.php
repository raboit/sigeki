<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,null,'post',array('id' => 'detalle-producto-form',)); ?>
    <fieldset>
        <legend>Ingresar Producto</legend>
        <?php echo TbHtml::label('Escanear Producto/Servicio', 'texto'); ?>
        <?php echo TbHtml::textField('codigo_barra', '',array('id' =>'codigo_barra', 'maxlength'=>36)); ?>
        <?php echo TbHtml::checkBox('no_aplicable', false, array('label' => 'NO APLICABLE')); ?>
        <?php echo TbHtml::submitButton('Añadir a la venta',array('value'=>true, 'name'=>'agregar', 'color' => TbHtml::BUTTON_COLOR_SUCCESS)); ?>
        <?php echo TbHtml::submitButton('Finalizar venta',array('value'=>true,'name'=>'terminar', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
    </fieldset>
<?php echo TbHtml::endForm(); ?>
<?php Yii::app()->clientScript->registerScript('focus','$(function(){$("#codigo_barra").focus();});',CclientScript::POS_READY);?>