<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'servicio-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => false,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->hiddenField($model,'id', array('span'=>3)); ?>
            <?php echo $form->textFieldControlGroup($model,'fecha', array('span'=>3, 'disabled'=>true)); ?>
        <?php echo $form->textFieldControlGroup($model,'tipoServicio', array('span'=>3, 'disabled'=>true)); ?>
		<?php echo $form->dropDownListControlGroup($model,'estilista_id', GxHtml::listDataEx(Estilista::model()->findAllAttributes(null, true)), array('class'=>'span3 selectpicker', 'empty'=>'Seleccione un estilista', 'placeholder'=> Yii::t('app', 'Select') ));?>
		<?php echo $form->dropDownListControlGroup($model,'formula_id', GxHtml::listDataEx(Formula::model()->findAllAttributes(null, true,'cliente_id=:cliente OR tipo="GENERICA"',array(':cliente'=>$model->cliente_id))), array('class'=>'span3 selectpicker', 'empty'=>'Seleccione una Formula', 'placeholder'=> Yii::t('app', 'Select') ));?>

<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
));
?>
<?php $this->endWidget(); ?>        
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>
