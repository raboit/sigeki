<?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
                'id' => 'detalle-venta-grid',
                'dataProvider' => new CArrayDataProvider($model->detalleVentas,array()),
                'type'=>'striped bordered condensed',
                'template'=>"{summary}{items}{pager}",
                'columns' => DetalleVenta::getColumnsVerVenta(),
            'mergeColumns' => array('tipodetalle'),
        )); ?>
<div class="row">
        <div class="well pull-right" style="width:150px;">
            <h4>Total Venta</h4>
            <h3>
                <div id="total_venta"><?php echo "$".Yii::app()->format->formatNumber($model->calcularTotal());?></div>
            </h3>
        </div>
</div>