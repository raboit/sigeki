<script type="text/javascript">
function agregarCliente()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("cliente/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#cliente_div').html(data.div);
                    $('#cliente_div').show();
                    $('#cliente_div form').submit(agregarCliente);
                }
                else
                {
                    $('#cliente_div').html(data.div);
                    actualizarCliente();
                    setTimeout(\"$('#myModal').modal('hide');\",500);
                    
                 }              
            }",
            ))?>;
    return false; 
 
} 

function actualizarCliente()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('cliente/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'cliente_id'),
            ))?>;
    return false; 
 
} 
</script>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'venta-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => true,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldControlGroup($model,'fecha', array('span'=>3, 'class'=>'datepicker','disabled' => true)); ?>
               	<?php echo $form->dropDownListControlGroup($model,'cliente_id', GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true,array('order'=>'nombre ASC','condition'=>'nombre!="Generica"'))), array('class'=>'selectpicker span3', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?>
                
<!--BOTONES DE FORMULARIO-->
<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Crear Venta') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
    TbHtml::linkButton('Nuevo cliente', array(
                    'icon'=>'plus white',
                    'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                    'onclick'=>'js:agregarCliente()',
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    #'url' => Yii::app()->createUrl('venta/crearDetalleVenta',array('id'=>$model->id)),
                    )),
));
?>
<?php $this->endWidget(); ?> 
<?php $this->widget('ext.select2.ESelect2'); ?>
<?php $this->widget('ext.datepicker.Datepicker'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'myModal',
    'header' => 'Crear nuevo cliente',
    'content' => '<div id="cliente_div"></div>',
)); ?>