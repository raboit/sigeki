<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
    array('label'=>Yii::t('app', 'Venta')),
        array('label'=>Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('ver','id' => $model->id), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(),'visible'=>(Yii::app()->user->getIsSuperuser())?true:false, 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),        
    array('label'=>Yii::t('app', 'Operaciones')),
        (Yii::app()->user->getIsSuperuser())?array('label'=>Yii::t('app', 'Anular'),'visible'=>!($model->estado=="ANULADA"), 'url'=>array('venta/anular','id' => $model->id), 'icon'=>'remove'):array(),
    array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>
<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php
$this->renderPartial('detalle_venta_producto/_formulario');
?>
<?php
$model_detalle_ventas=new CArrayDataProvider($model->detalleVentas,array( 'pagination'=>array('pageSize'=>20)));
?>
<?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
	'id' => 'detalle-venta-grid',
	'dataProvider' => $model_detalle_ventas,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}\n",
	'columns' => DetalleVenta::getColumnsCrearDetalleVenta(),
        'mergeColumns' => array('tipodetalle'),
)); ?>

<div class="row">
        <div class="well pull-right" style="width:150px;">
            <h4>Total Venta</h4>
            <h3><div id="total_venta"><?php echo "$".Yii::app()->format->formatNumber($model->calcularTotal());?></div></h3>
        </div>
</div>