<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'venta-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => false,
));
?>
<?php Yii::app()->clientScript->registerScript('focus_pagar','$(function(){$("#codigo_barra").focus();});',CclientScript::POS_READY);?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
                <?php echo $form->textFieldControlGroup($model,'cantidad_pago', array('span'=>3,'onkeyup'=>'js:$("#'.CHtml::activeId($model,"cantidad_vuelto").'").val(this.value-'.$model->calcularTotal().')')); ?>
		<?php echo $form->textFieldControlGroup($model,'total', array('span'=>3, 'class'=>'datepicker','disabled'=>true)); ?>
                <?php echo $form->textFieldControlGroup($model,'cantidad_vuelto', array('span'=>3, 'class'=>'datepicker','disabled'=>true)); ?>
		<?php echo $form->dropDownListControlGroup($model,'tipo_pago', array('Visa'=>'Visa','Cheque'=>'Cheque','Efectivo'=>'Efectivo'), array('class'=>'selectpicker', 'empty'=>'Seleccione un tipo de pago', 'placeholder'=> Yii::t('app', 'Select') ));?>
                
<!--BOTONES DE FORMULARIO-->
<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Guardar pago'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
));
?>
<?php $this->endWidget(); ?> 