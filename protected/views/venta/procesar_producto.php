<?php


?>
<?php
$model=new Venta;
$this->breadcrumbs = array(
	$model->label(2) => 'javascript:history.back()',
	'Procesar producto' => '',
);

$this->menu=array(             
    array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>
<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,null,'post',array('id' =>'detalle-producto-form','onSubmit'=>'')); ?>
<fieldset>
        <legend>Procesar Producto</legend>
        <?php echo TbHtml::label('Escanear Producto/Servicio', 'texto'); ?>
        <?php echo TbHtml::textField('codigo_barra', '',array('id' =>'codigo_barra', 'maxlength'=>36)); ?><br>
        <?php echo TbHtml::submitButton('Procesar producto',array('value'=>true, 'name'=>'agregar', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo TbHtml::linkButton('Finalizar',array('url'=>Yii::app()->createUrl('venta'),'value'=>true, 'name'=>'agregar', 'color' => TbHtml::BUTTON_COLOR_SUCCESS)); ?>
</fieldset>
<?php echo TbHtml::endForm(); ?>
<?php Yii::app()->clientScript->registerScript('focus','$(function(){$("#codigo_barra").focus();});',CclientScript::POS_READY);?>
<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php if($model_producto):?>    
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model_detalle_producto,
	'attributes' => array(
		'id',
		'codigo_barra',
		'fecha_creacion',
		'estado',
		'fecha_modificacion',
		array(
			'name' => 'producto',
			'type' => 'raw',
			'value' => $model_detalle_producto->producto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model_detalle_producto->producto)), array('producto/ver', 'id' => GxActiveRecord::extractPkValue($model_detalle_producto->producto, true))) : null,
			),
		array(
			'name' => 'compra',
			'type' => 'raw',
			'value' => $model_detalle_producto->compra !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model_detalle_producto->compra)), array('compra/ver', 'id' => GxActiveRecord::extractPkValue($model_detalle_producto->compra, true))) : null,
			),
	),
)); ?>

<?php endif;?>