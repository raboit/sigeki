<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('view', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
        <?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
        <?php echo GxHtml::encode($data->total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cajero_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cajero)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('turno_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->turno)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cliente)); ?>
	<br />

</div>
</a>