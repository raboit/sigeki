<script type="text/javascript">
    function asignarEstilista(){
        <?php echo CHtml::ajax(array(
                'url'=>Yii::app()->createUrl("venta/asignarEstilista",array('id'=>$model->id)),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#asignacion_div').html(data.div);
                        $('#asignacion_div').show();
                        $('#asignacion_div form').submit(asignarEstilista);
                    }
                    else
                    {
                        $('#asignacion_div').html(data.mensaje);
                        $('#detalles_venta').html(data.div_detalle);
                        $('#informacion_venta').html(data.div_venta);
                        $('#myModal2').modal('hide');
                        ingresarPago();
                        $('#myModal').modal('show');

                     }              
                }",
                ))
        ?>;
        return false; 
    }
    
    function ingresarPago()
    {
        <?php echo CHtml::ajax(array(
                'url'=>Yii::app()->createUrl("venta/pagarVenta",array('id'=>$model->id)),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#cliente_div').html(data.div);
                        $('#cliente_div').show();
                        $('#cliente_div form').submit(ingresarPago);
                    }
                    else
                    {
                        $('#cliente_div').html(data.mensaje);
                        $('#informacion_venta').html(data.div);
                        setTimeout(\"$('#myModal').modal('hide');\",500);

                     }              
                }",
                ))
        ?>;
        return false;  
    } 
</script>


<?php

$this->breadcrumbs = array(
	$model->label(2) => 'javascript:history.back()',
	GxHtml::valueEx($model),
);

$this->menu=array(
        
    array('label'=>Yii::t('app', 'Venta')),   
        array('label'=>Yii::t('app', 'Create') . ' ' . Venta::label(), 'url' => array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . Venta::label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . Venta::label(), 'url'=>'#','visible'=>(Yii::app()->user->getIsSuperuser())?true:false, 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        
    array('label'=>Yii::t('app', 'Operaciones')),
        array('label'=>Yii::t('app', 'Agregar detalle'),'visible'=>($model->estado=="NUEVA"), 'url'=>array('venta/crearDetalleVenta','id' => $model->id), 'icon'=>'shopping-cart'),
        array('label'=>Yii::t('app', 'Pagar'),'visible'=>($model->estado=="NUEVA"),'url'=>'#', 'data-toggle' => 'modal','data-target' => '#myModal','icon'=>'ok'),
        (Yii::app()->user->getIsSuperuser())?array('label'=>Yii::t('app', 'Anular'),'visible'=>!($model->estado=="ANULADA"), 'url'=>array('venta/anular','id' => $model->id), 'icon'=>'remove'):array(),
           
    array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>
<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>
<div id="informacion_venta">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		//'id',
		'fecha',
                array(
			'name' => 'cajero',
			'type' => 'raw',
			'value' => $model->cajero !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cajero)), array('cajero/ver', 'id' => GxActiveRecord::extractPkValue($model->cajero, true))) : null,
			),
                array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),

                array(
			'name' => 'total',
			'type' => 'raw',
			'value' => Yii::app()->format->formatNumber($model->calcularTotal()),
			),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>$model->getLabel()),
            
	),
)); ?>
<!--        BOTONES DE DETALLE DE VENTA-->
        <?php   $model->menu_botones(); ?>    
</div>
<div id="detalles_venta">
        <?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
                'id' => 'detalle-venta-grid',
                'dataProvider' => new CArrayDataProvider($model->detalleVentas,array()),
                'type'=>'striped bordered condensed',
                'template'=>"{summary}{items}{pager}",
                'columns' => DetalleVenta::getColumnsVerVenta(),
            'mergeColumns' => array('tipodetalle'),
        )); ?>
<div class="row">
        <div class="well pull-right" style="width:150px;">
            <h4>Total Venta</h4>
            <h3>
                <div id="total_venta"><?php echo "$".Yii::app()->format->formatNumber($model->calcularTotal());?></div>
            </h3>
        </div>
</div>
</div>
<?php $this->widget('bootstrap.widgets.TbModal', array(
            'id' => 'myModal',
            'header' => 'Pagar cuenta',
            'content' => '<div id="cliente_div"></div>',
        )); ?>
<?php $this->widget('bootstrap.widgets.TbModal', array(
            'id' => 'myModal2',
            'header' => 'Asignar estilista',
            'content' => '<div id="asignacion_div"></div>',
        )); ?>