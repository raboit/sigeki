<?php

$this->breadcrumbs = array(
	Venta::label(2),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' ' . Venta::label(), 'url' => array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . Venta::label(2),'visible'=>(Yii::app()->user->getIsSuperuser())?true:false, 'url' => array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>
<?php echo TbHtml::pageHeader(GxHtml::encode(Venta::label(2)), null); ?>


<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('venta/crear');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Crear nueva venta" ?></h4>

                        <?php echo "Vender productos y servicios" ?>
                         
                        </span>
                </div>
        </a>
</div>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('venta/administrar');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Administrar ventas" ?></h4>

                        <?php echo "Ver ventas realizadas" ?>
                         
                        </span>
                </div>
        </a>
</div>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/scroll.js'); ?>