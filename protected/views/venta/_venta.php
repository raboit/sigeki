<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'fecha',
            		array(
			'name' => 'cajero',
			'type' => 'raw',
			'value' => $model->cajero !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cajero)), array('cajero/ver', 'id' => GxActiveRecord::extractPkValue($model->cajero, true))) : null,
			),
                array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),

                array(
			'name' => 'total',
			'type' => 'raw',
			'value' => Yii::app()->format->formatNumber($model->calcularTotal()),
			),
                array('type'=>'raw','name'=>'estado','header'=>'Estado','value'=>$model->getLabel()),
            
	),
)); ?>

<!--        BOTONES DE DETALLE DE VENTA-->
        <?php   $model->menu_botones(); ?>
