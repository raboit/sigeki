<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app',  'Inicio') , 'url'=>array('index'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Export'),'visible'=>(Yii::app()->user->getIsSuperuser())?true:false),
        array('label'=>Yii::t('app', 'Export to Excel'),'visible'=>(Yii::app()->user->getIsSuperuser())?true:false, 'url'=>Yii::app()->controller->createUrl('GenerarExcel'), 'linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<div class="title-menu">
        <?php echo TbHtml::pageHeader(Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)), null); ?>
</div>

<p>
<?php echo Yii::t('app', 'Text Option Search'); ?></p>

<div class="buttons-admin">
<?php 
       echo CHtml::link(Yii::t('app', 'Advanced Search'),'#',array('class'=>'search-button btn'));
?>
</div>
<div class="search-form">
<?php $this->renderPartial('_busqueda', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'venta-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{extendedSummary}{items}{pager}",        
	'columns' => Venta::getColumns(),
        'extendedSummary' => array(
            'title' => 'Total',
            'columns' => array(
            'total' => array(
                    'label'=>'Total',
                    'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
	),)),
            
        'extendedSummaryOptions' => array(
            'class' => 'well pull-right',
            'style' => 'width:300px'
	),
        
    
)); ?>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('venta-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>