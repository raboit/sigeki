<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class DetalleProductoBehavior extends CBehavior{

    private $owner;
    public function getLabel(){
            $this->owner = $this->getOwner();
            if($this->owner->estado=="ANULADA")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
            elseif($this->owner->estado=="PROCESO")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_WARNING));
            elseif($this->owner->estado=="VENDIDO")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_INFO));
            else
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_SUCCESS));
        }
}

?>
