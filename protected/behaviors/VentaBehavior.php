<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class VentaBehavior extends CBehavior{
    private $owner;
    public $cantidad_pago;
    public $cantidad_vuelto;

    /*
     * funcion que muestra el precio total de una venta
     */
        public function calcularTotal(){
            $this->owner = $this->getOwner();
            $total=0;
            foreach ($this->owner->detalleVentas as $detalle) {
                $total=$total+$detalle->precio;                
            }
            return $total;
        }
        /*
         * Funcion que despliega el estado de una venta
         */
        public function getLabel(){
            $this->owner = $this->getOwner();
            if($this->owner->estado=="ANULADA")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
            if($this->owner->estado=="PAGADA")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_SUCCESS));
            else
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_INFO));
        }
        /*
         * Funcion que despliega los botones de operaciones disponibles  para una venta
         */
        public function menu_botones(){
            $this->owner = $this->getOwner();
            if($this->owner->estado=="NUEVA" OR $this->owner->estado=="FINALIZADA"){            
                echo TbHtml::linkButton('Agregar Detalle', array(
                    'icon'=>'shopping-cart white',
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'url' => Yii::app()->createUrl('venta/crearDetalleVenta',array('id'=>$this->owner->id)),
                    ))." ";            
                echo TbHtml::linkButton('Pagar', array(
                  'icon'=>'ok white',
                  'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                  'onclick'=>'js:ingresarPago()',
                  'data-toggle' => 'modal',
                  'data-target' => '#myModal',
                  ))." ";  
            }
            
            if($this->owner->estado!="ANULADA" AND (Yii::app()->user->getIsSuperuser())){
                echo TbHtml::linkButton('Anular venta', array(
                    'icon'=>'remove white',
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'url' => Yii::app()->createUrl('venta/anular',array('id'=>$this->owner->id)),
                    ))." ";   
            }
             if(!$this->owner->verificarServicios()){
                  echo TbHtml::linkButton('Asignar estilista', array(
                    'icon'=>'exclamation-sign white',
                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                    'onclick'=>'js:asignarEstilista()',
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal2',
                    ));
             }
            
        }

        /*
         * Funcion para verificar los servicios especificados en una venta
         * si encuentra un servicio que aun no se le ha asignado un estilista
         * devolvera falso(0), caso contrario es true(1)
         * Los servicios no aplicable no seran tomados en cuenta
         */
        public function verificarServicios(){
            $this->owner = $this->getOwner();
            foreach ($this->owner->detalleVentas(array('scopes'=>array('servicios_no_asignados'))) as $detalle):
                    return 0;                  
            endforeach;
            
            return 1;
        }    
        /*
         * Funcion para cambiar el estado de los detalles de venta de una Venta
         * recibe el nombre de la funcion que dese desea ejecutar
         * Estados de producto: NUEVO PROCESO TERMINADO
         */
        public function actualizarStock($function="pagar"){
            $this->$function();
        }
        /*
         * Function que descuenta del stock actual lo detallado en la venta
         * solo aplica para productos
         */
        public function pagar(){
            $this->owner = $this->getOwner();
            foreach ($this->owner->detalleVentas(array('scopes'=>array('productos'))) as $detalle):
                if(!$detalle->no_aplicable){
                    $detalle->producto->cantidad_unidades=$detalle->producto->cantidad_unidades-1;
                    $detalle->producto->save(false);
                }
                
            endforeach;    
        }
        
        /*
         * Function que devuelve al stock actual lo detallado en la venta
         * aplica para productos y productos utilizados en servicios
         * los servicios que se encuentren finalizados no seran tomados en cuenta
         */
        public function anular(){
            $this->owner = $this->getOwner();
            foreach ($this->owner->detalleVentas(array('scopes'=>array('productos'))) as $detalle):
                if(!$detalle->no_aplicable){
                    $detalle->producto->cantidad_unidades=$detalle->producto->cantidad_unidades+1;
                    $detalle->producto->save(false);
                }                
            endforeach;
            foreach ($this->owner->detalleVentas(array('scopes'=>array('servicios_no_asignados'))) as $detalle):
                if(isset($detalle->servicio) AND isset($detalle->servicio->formula) AND isset($detalle->servicio->formula->componentes)){
                    foreach ($detalle->servicio->formula->componentes as $componente) {
                        $componente->producto->cantidad_gramos = $componente->producto->cantidad_gramos + $componente->cantidad_gramo;
                        $componente->producto->save(true);
                    }}
                $detalle->servicio->estado="ANULADA";
                $detalle->servicio->save(true);
            endforeach;
            
        }
}
?>
