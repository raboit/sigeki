<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ServicioBehavior extends CBehavior{
    private $owner;
    
    public function getLabel(){
            $this->owner = $this->getOwner();
            if($this->owner->estado=="FINALIZADO")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_SUCCESS));
            elseif($this->owner->estado=="NUEVO")
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_INFO));
            else
                return TbHtml::labelTb($this->owner->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
        }
        
        public function calcularTotal(){
            $this->owner = $this->getOwner();
            $total=0;
            foreach ($this->owner->detalleVentas as $detalle) {
                $total=$total+$detalle->precio;                
            }
            return $total;
        }
}
?>
