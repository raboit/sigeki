<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class DetalleVentaBehavior extends CBehavior{
    private $owner;
    public $tipodetalle;
    public $nombredetalle;
    
        public function getTipoDetalle(){
            $this->owner = $this->getOwner();
            if($this->owner->servicio_id){
                return "Servicio";
            }else{
                return "Producto";
            }
        }
        
        public function getAsignacion(){
            $this->owner = $this->getOwner();
            if($this->owner->servicio_id){
                if($this->owner->servicio->estado=="NUEVO")
                    return TbHtml::labelTb($this->owner->servicio->estado, array("color" => TbHtml::LABEL_COLOR_WARNING));
                
                if($this->owner->servicio->estado=="FINALIZADO")
                    return TbHtml::labelTb($this->owner->servicio->estado, array("color" => TbHtml::LABEL_COLOR_INFO));
                
                if($this->owner->servicio->estado=="N/A")
                    return TbHtml::labelTb($this->owner->servicio->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
                
                if($this->owner->servicio->estado=="ANULADA")
                    return TbHtml::labelTb($this->owner->servicio->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
            }else{
                return TbHtml::labelTb("N/A", array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
            }
        }
        
        public function getNombreDetalle(){
            $this->owner = $this->getOwner();
            if(isset($this->owner->servicio_id))
                return $this->owner->servicio->tipoServicio->nombre;
            else
                return $this->owner->producto->nombre;
            
        }
        
        
        
}
?>