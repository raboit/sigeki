-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-11-2013 a las 13:15:20
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sigeki`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Administrador', '1', NULL, 'N;'),
('Cajero', '12', NULL, 'N;'),
('Cajero', '14', NULL, 'N;'),
('Estilista', '11', NULL, 'N;'),
('Estilista', '13', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Administrador', 2, NULL, NULL, 'N;'),
('Cajero', 2, 'Cajero', NULL, 'N;'),
('Cliente.Agregar', 0, NULL, NULL, 'N;'),
('Cliente.GetTodos', 0, NULL, NULL, 'N;'),
('DetalleVenta.Borrar', 0, NULL, NULL, 'N;'),
('DetalleVenta.Ver', 0, NULL, NULL, 'N;'),
('Estilista', 2, 'Estilista', NULL, 'N;'),
('Invitado', 2, NULL, NULL, 'N;'),
('Servicio.Actualizar', 0, NULL, NULL, 'N;'),
('Servicio.Ver', 0, NULL, NULL, 'N;'),
('Test.*', 1, NULL, NULL, 'N;'),
('Test.Index', 0, NULL, NULL, 'N;'),
('Usuario', 2, NULL, NULL, 'N;'),
('Venta', 1, 'Venta', NULL, 'N;'),
('Venta.Actualizar', 0, NULL, NULL, 'N;'),
('Venta.Administrar', 0, NULL, NULL, 'N;'),
('Venta.Anular', 0, NULL, NULL, 'N;'),
('Venta.Crear', 0, NULL, NULL, 'N;'),
('Venta.CrearDetalleVenta', 0, NULL, NULL, 'N;'),
('Venta.GetTotalventa', 0, NULL, NULL, 'N;'),
('Venta.Index', 0, NULL, NULL, 'N;'),
('Venta.PagarVenta', 0, NULL, NULL, 'N;'),
('Venta.ProcesarProducto', 0, NULL, NULL, 'N;'),
('Venta.Ver', 0, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('Venta', 'Cliente.Agregar'),
('Venta', 'Cliente.GetTodos'),
('Cajero', 'DetalleVenta.Borrar'),
('Venta', 'DetalleVenta.Borrar'),
('Venta', 'DetalleVenta.Ver'),
('Venta', 'Servicio.Actualizar'),
('Venta', 'Servicio.Ver'),
('Cajero', 'Venta.Actualizar'),
('Venta', 'Venta.Actualizar'),
('Cajero', 'Venta.Administrar'),
('Venta', 'Venta.Administrar'),
('Venta', 'Venta.Anular'),
('Cajero', 'Venta.Crear'),
('Venta', 'Venta.Crear'),
('Cajero', 'Venta.CrearDetalleVenta'),
('Venta', 'Venta.CrearDetalleVenta'),
('Cajero', 'Venta.GetTotalventa'),
('Venta', 'Venta.GetTotalventa'),
('Cajero', 'Venta.Index'),
('Venta', 'Venta.Index'),
('Cajero', 'Venta.PagarVenta'),
('Venta', 'Venta.PagarVenta'),
('Cajero', 'Venta.ProcesarProducto'),
('Cajero', 'Venta.Ver'),
('Venta', 'Venta.Ver');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajero`
--

CREATE TABLE IF NOT EXISTS `cajero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rut` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` int(11) DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cajero_user1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcar la base de datos para la tabla `cajero`
--

INSERT INTO `cajero` (`id`, `nombre`, `rut`, `celular`, `email`, `user_id`) VALUES
(9, 'Cajero', NULL, NULL, NULL, 12),
(10, 'rap', NULL, NULL, NULL, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Volcar la base de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `email`, `celular`, `direccion`, `fecha_nacimiento`) VALUES
(18, 'Gonzalo Godoy', NULL, '98778965', 'Los Limoneros #2432', '1900-12-29'),
(19, 'Juan Carrasco', NULL, '90988762', 'El Roble #3903', '1984-10-11'),
(20, 'Cliente 3', NULL, '90988775', 'Buin 4523', '1900-12-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componente`
--

CREATE TABLE IF NOT EXISTS `componente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_gramo` int(11) DEFAULT NULL,
  `formula_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_componente_formula1` (`formula_id`),
  KEY `fk_componente_producto1` (`producto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `componente`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE IF NOT EXISTS `compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_productos` int(11) DEFAULT NULL,
  `precio_compra` int(11) DEFAULT NULL,
  `fecha_compra` datetime DEFAULT NULL,
  `producto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_compra_producto1` (`producto_id`),
  KEY `fk_compra_proveedor1` (`proveedor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcar la base de datos para la tabla `compra`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE IF NOT EXISTS `detalle_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `no_aplicable` tinyint(4) DEFAULT NULL,
  `venta_id` int(11) NOT NULL,
  `servicio_id` int(11) DEFAULT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detalle_venta_venta1` (`venta_id`),
  KEY `fk_detalle_venta_servicio1` (`servicio_id`),
  KEY `fk_detalle_venta_producto1` (`producto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=109 ;

--
-- Volcar la base de datos para la tabla `detalle_venta`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estilista`
--

CREATE TABLE IF NOT EXISTS `estilista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `porcentaje` float DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_estilista_user1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `estilista`
--

INSERT INTO `estilista` (`id`, `nombre`, `porcentaje`, `email`, `user_id`) VALUES
(3, 'Estilista 1', NULL, NULL, 11),
(4, 'Estilista 2', NULL, NULL, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formula`
--

CREATE TABLE IF NOT EXISTS `formula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_edicion` datetime DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'GENERICA',
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_formula_cliente1` (`cliente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `formula`
--

INSERT INTO `formula` (`id`, `nombre`, `fecha_creacion`, `fecha_edicion`, `tipo`, `cliente_id`) VALUES
(3, 'Castoño Claro Verano', '2013-10-18 13:22:26', NULL, 'GENERICA', 18),
(4, 'Castaño Oscuro Invierno', '2013-10-18 13:22:45', NULL, 'GENERICA', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gramos_servicio`
--
-- en uso(#1356 - View 'sigeki.gramos_servicio' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

--
-- Volcar la base de datos para la tabla `gramos_servicio`
--
-- en uso (#1356 - View 'sigeki.gramos_servicio' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad_gramos` int(11) DEFAULT '0',
  `cantidad_unidades` int(11) DEFAULT '0',
  `precio_venta` int(11) DEFAULT NULL,
  `codigo_barra` int(11) DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'TERMINADO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcar la base de datos para la tabla `producto`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_comercial` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rut` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `nombre_comercial`, `rut`, `telefono`, `direccion`) VALUES
(2, 'Proveedor 1', NULL, '2 244154', 'San Marcos #3445'),
(3, 'Proveedor 2', NULL, '2223144', 'Buin 1825'),
(4, 'Proveedor 3', NULL, '223345', 'Arica 33554');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `rights`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE IF NOT EXISTS `servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NUEVO',
  `estilista_id` int(11) NOT NULL,
  `formula_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) NOT NULL,
  `tipo_servicio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_servicio_estilista1` (`estilista_id`),
  KEY `fk_servicio_tipo_servicio1` (`tipo_servicio_id`),
  KEY `fk_servicio_cliente1` (`cliente_id`),
  KEY `fk_servicio_formula1` (`formula_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Volcar la base de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `fecha`, `estado`, `estilista_id`, `formula_id`, `cliente_id`, `tipo_servicio_id`) VALUES
(49, NULL, 'NUEVO', 3, 3, 18, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_servicio`
--

CREATE TABLE IF NOT EXISTS `tipo_servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_barra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio_venta` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `tipo_servicio`
--

INSERT INTO `tipo_servicio` (`id`, `nombre`, `codigo_barra`, `precio_venta`) VALUES
(4, 'Lavado Capilar Extra Fino', '555444', 5000),
(5, 'Tintura de Cabello', '555555', NULL),
(6, 'Alizamiento de Cabello', '555666', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE IF NOT EXISTS `turno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_termino` datetime DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVO',
  `cajero_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_turno_cajero` (`cajero_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=63 ;

--
-- Volcar la base de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `fecha_inicio`, `fecha_termino`, `estado`, `cajero_id`) VALUES
(61, '2013-10-15 13:59:31', '2013-10-21 13:07:14', 'CERRADO', 9),
(62, '2013-10-21 13:07:14', NULL, 'ACTIVO', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `tipo`, `activo`, `rol`) VALUES
(1, 'admin', 'c63b447bcee0c5064cac9d264e62114e2d50516e', '', NULL, 1, 'Administrador'),
(11, 'estilista1@kikasalon.cl', '756dbae8b5b585b8e8b4dc50b4c0d7fbca3f4322', 'estilista@kikasalon.cl', NULL, 1, 'Estilista'),
(12, 'cajero@kikasalon.cl', 'e3b12e138c3f95077f8798eb106e405d3aacf536', 'cajero@kikasalon.cl', NULL, 1, 'Cajero'),
(13, 'estilista2@kikasalon.cl', '7e4f53c48f2e434859b406a6c6a953376202ffd4', 'estilista2@kikasalon.cl', NULL, 1, 'Estilista'),
(14, 'rap@rap.cl', 'ff5522be2d011958eb9e4c8c4cf9daceca3721f9', 'rap@rap.cl', NULL, 1, 'Cajero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE IF NOT EXISTS `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NUEVA',
  `total` int(11) DEFAULT NULL,
  `tipo_pago` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad_pago` int(11) DEFAULT NULL,
  `cajero_id` int(11) NOT NULL,
  `turno_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_venta_cajero1` (`cajero_id`),
  KEY `fk_venta_turno1` (`turno_id`),
  KEY `fk_venta_cliente1` (`cliente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Volcar la base de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `fecha`, `estado`, `total`, `tipo_pago`, `cantidad_pago`, `cajero_id`, `turno_id`, `cliente_id`) VALUES
(50, '2013-11-14 13:36:41', 'NUEVA', NULL, NULL, NULL, 9, 62, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `yiilog`
--

CREATE TABLE IF NOT EXISTS `yiilog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(128) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `logtime` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Volcar la base de datos para la tabla `yiilog`
--

INSERT INTO `yiilog` (`id`, `level`, `category`, `logtime`, `message`) VALUES
(1, 'error', 'system.db.CDbCommand', 1377127746, 'CDbCommand::execute() falló: SQLSTATE[42S02]: Base table or view not found: 1146 Table ''sigeki.yiilog'' doesn''t exist. La sentencia SQL ejecutada fue: DELETE FROM `YiiLog` WHERE 0=1.'),
(2, 'error', 'system.db.CDbCommand', 1377128194, 'CDbCommand::fetchAll() falló: SQLSTATE[42S02]: Base table or view not found: 1146 Table ''sigeki.authassignment'' doesn''t exist. La sentencia SQL ejecutada fue: SELECT *\nFROM `AuthAssignment`\nWHERE userid=:userid. Bound with :userid=NULL.'),
(3, 'error', 'exception.CDbException', 1377128194, 'exception ''CDbException'' with message ''CDbCommand falló al ejecutar la sentencia SQL: SQLSTATE[42S02]: Base table or view not found: 1146 Table ''sigeki.authassignment'' doesn''t exist'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\db\\CDbCommand.php:541\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\db\\CDbCommand.php(395): CDbCommand->queryInternal(''fetchAll'', Array, Array)\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\auth\\CDbAuthManager.php(337): CDbCommand->queryAll()\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\auth\\CDbAuthManager.php(74): CDbAuthManager->getAuthAssignments(NULL)\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\auth\\CWebUser.php(806): CDbAuthManager->checkAccess(''Test.*'', NULL, Array)\n#4 C:\\xampp\\htdocs\\sigeki\\backend\\modules\\rights\\components\\RWebUser.php(43): CWebUser->checkAccess(''Test.*'', Array, true)\n#5 C:\\xampp\\htdocs\\sigeki\\backend\\modules\\rights\\components\\RightsFilter.php(43): RWebUser->checkAccess(''Test.*'')\n#6 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CFilter.php(38): RightsFilter->preFilter(Object(CFilterChain))\n#7 C:\\xampp\\htdocs\\sigeki\\backend\\modules\\rights\\components\\RController.php(36): CFilter->filter(Object(CFilterChain))\n#8 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CInlineFilter.php(58): RController->filterRights(Object(CFilterChain))\n#9 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CFilterChain.php(130): CInlineFilter->filter(Object(CFilterChain))\n#10 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(291): CFilterChain->run()\n#11 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(265): CController->runActionWithFilters(Object(CInlineAction), Array)\n#12 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run('''')\n#13 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''test'')\n#14 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#15 C:\\xampp\\htdocs\\sigeki\\backend\\www\\index.php(24): CApplication->run()\n#16 {main}\nREQUEST_URI=/sigeki/backend/www/test\n---'),
(4, 'error', 'exception.CHttpException.404', 1377128546, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "index" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction('''')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run('''')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''test'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/test\n---'),
(5, 'error', 'exception.CHttpException.404', 1377128959, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "cajero"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''cajero'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/cajero\n---'),
(6, 'error', 'exception.CHttpException.404', 1377129372, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "index" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction('''')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run('''')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''producto'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/producto\n---'),
(7, 'error', 'exception.CHttpException.404', 1377129685, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(8, 'error', 'exception.CHttpException.404', 1377129711, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(9, 'error', 'exception.CHttpException.404', 1377129780, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(10, 'error', 'exception.CHttpException.404', 1377129796, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(11, 'error', 'exception.CHttpException.404', 1377129874, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(12, 'error', 'exception.CHttpException.404', 1377129969, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(13, 'error', 'exception.CHttpException.404', 1377130123, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "index" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction('''')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run('''')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''cliente'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/cliente\n---'),
(14, 'error', 'exception.CHttpException.404', 1377153904, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(15, 'error', 'exception.CHttpException.404', 1377153939, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(16, 'error', 'exception.CHttpException.404', 1377153954, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(17, 'error', 'exception.CHttpException.404', 1377154024, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(18, 'error', 'exception.CHttpException.404', 1377154095, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(19, 'error', 'exception.CHttpException.404', 1377154122, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(20, 'error', 'exception.CHttpException.404', 1377154236, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(21, 'error', 'exception.CHttpException.404', 1377154245, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(22, 'error', 'exception.CHttpException.404', 1377154277, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(23, 'error', 'exception.CHttpException.404', 1377154300, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(24, 'error', 'exception.CHttpException.404', 1377154342, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(25, 'error', 'exception.CHttpException.404', 1377154677, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(26, 'error', 'exception.CHttpException.404', 1377154799, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(27, 'error', 'exception.CHttpException.404', 1377154817, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(28, 'error', 'exception.CHttpException.404', 1377154837, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/administrar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/adminis...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/administrar\n---'),
(29, 'error', 'exception.CHttpException.404', 1377154841, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/listar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/listar'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/\n---'),
(30, 'error', 'exception.CHttpException.404', 1377154841, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/administrar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/adminis...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/administrar\n---'),
(31, 'error', 'exception.CHttpException.404', 1377154846, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "listar" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction(''listar'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run(''listar'')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''user/listar'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/user/\n---'),
(32, 'error', 'exception.CHttpException.404', 1377155088, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "misClientes" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction(''misClientes'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run(''misClientes'')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''cliente/misClie...'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/cliente/misClientes\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(33, 'error', 'exception.CHttpException.404', 1377155215, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "detalle_producto/administrar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''detalle_product...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/detalle_producto/administrar\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(34, 'error', 'exception.CHttpException.404', 1377155263, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "detalle_producto/administrar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''detalle_product...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/detalle_producto/administrar\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(35, 'error', 'exception.CHttpException.404', 1377155383, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(36, 'error', 'exception.CHttpException.404', 1377155469, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(37, 'error', 'exception.CHttpException.404', 1377155485, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\n---'),
(38, 'error', 'exception.CException', 1377876438, 'exception ''CException'' with message ''Cliente y sus behaviors no tienen un método o closure llamado "search".'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CComponent.php:265\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\db\\ar\\CActiveRecord.php(225): CComponent->__call(''search'', Array)\n#1 C:\\xampp\\htdocs\\sigeki\\frontend\\controllers\\ClienteController.php(106): CActiveRecord->__call(''search'', Array)\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\controllers\\ClienteController.php(106): Cliente->search()\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\actions\\CInlineAction.php(49): ClienteController->actionGenerarExcel()\n#4 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(308): CInlineAction->runWithParams(Array)\n#5 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CFilterChain.php(133): CController->runAction(Object(CInlineAction))\n#6 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CFilter.php(40): CFilterChain->run()\n#7 C:\\xampp\\htdocs\\sigeki\\backend\\modules\\rights\\components\\RController.php(36): CFilter->filter(Object(CFilterChain))\n#8 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CInlineFilter.php(58): RController->filterRights(Object(CFilterChain))\n#9 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\filters\\CFilterChain.php(130): CInlineFilter->filter(Object(CFilterChain))\n#10 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(291): CFilterChain->run()\n#11 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(265): CController->runActionWithFilters(Object(CInlineAction), Array)\n#12 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run(''GenerarExcel'')\n#13 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''cliente/Generar...'')\n#14 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#15 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#16 {main}\nREQUEST_URI=/sigeki/frontend/www/cliente/GenerarExcel\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(39, 'error', 'exception.CHttpException.404', 1377908662, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "themes/pixel/js/dist/jquery-migrate.min.map"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''themes/pixel/js...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/themes/pixel/js/dist/jquery-migrate.min.map\nHTTP_REFERER=http://localhost/sigeki/frontend/www/\n---'),
(40, 'error', 'exception.CHttpException.404', 1377910489, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "sigeki/frontend/www/cliente/ver/id/1"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''sigeki/frontend...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/sigeki/frontend/www/cliente/ver/id/1\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(41, 'error', 'exception.CHttpException.404', 1377910618, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "ver/id"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''ver/id'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/ver/id/1\nHTTP_REFERER=http://localhost/sigeki/frontend/www/cliente/administrar\n---'),
(42, 'error', 'exception.CHttpException.404', 1377911351, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/crear"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/crear'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/crear\n---'),
(43, 'error', 'exception.CHttpException.404', 1377911355, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/administrar"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/adminis...'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/administrar\n---'),
(44, 'error', 'exception.CHttpException.404', 1377911359, 'exception ''CHttpException'' with message ''No es posible resolver la solicitud "usuario/admin"'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php:286\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''usuario/admin'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#2 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#3 {main}\nREQUEST_URI=/sigeki/frontend/www/usuario/admin\n---'),
(45, 'error', 'exception.CHttpException.404', 1377911367, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "crear" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction(''crear'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run(''crear'')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''user/crear'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/user/crear\n---'),
(46, 'error', 'exception.CHttpException.404', 1377911370, 'exception ''CHttpException'' with message ''El sistema no ha podido encontrar la acción "listar" solicitada.'' in C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php:483\nStack trace:\n#0 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CController.php(270): CController->missingAction(''listar'')\n#1 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(282): CController->run(''listar'')\n#2 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\web\\CWebApplication.php(141): CWebApplication->runController(''user/listar'')\n#3 C:\\xampp\\htdocs\\sigeki\\common\\lib\\vendor\\yiisoft\\yii\\framework\\base\\CApplication.php(169): CWebApplication->processRequest()\n#4 C:\\xampp\\htdocs\\sigeki\\frontend\\www\\index.php(27): CApplication->run()\n#5 {main}\nREQUEST_URI=/sigeki/frontend/www/user/\n---');

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `cajero`
--
ALTER TABLE `cajero`
  ADD CONSTRAINT `fk_cajero_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `componente`
--
ALTER TABLE `componente`
  ADD CONSTRAINT `fk_componente_formula1` FOREIGN KEY (`formula_id`) REFERENCES `formula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_componente_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compra_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `fk_detalle_venta_servicio1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta_venta1` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `estilista`
--
ALTER TABLE `estilista`
  ADD CONSTRAINT `fk_estilista_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `formula`
--
ALTER TABLE `formula`
  ADD CONSTRAINT `fk_formula_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD CONSTRAINT `fk_servicio_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicio_estilista1` FOREIGN KEY (`estilista_id`) REFERENCES `estilista` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicio_formula1` FOREIGN KEY (`formula_id`) REFERENCES `formula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicio_tipo_servicio1` FOREIGN KEY (`tipo_servicio_id`) REFERENCES `tipo_servicio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `turno`
--
ALTER TABLE `turno`
  ADD CONSTRAINT `fk_turno_cajero` FOREIGN KEY (`cajero_id`) REFERENCES `cajero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_cajero1` FOREIGN KEY (`cajero_id`) REFERENCES `cajero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_turno1` FOREIGN KEY (`turno_id`) REFERENCES `turno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
