<?php

class ClienteController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Cliente');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Cliente'),
		));
	}
        public function actionGetTodos(){
            $data= Cliente::model()->findAll(array('order'=>'id DESC'));
            $data=CHtml::listData($data,'id','nombre');
             foreach($data as $value=>$name):
                 echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
             endforeach;
        }

        public function actionAgregar(){                
            $model = new Cliente;            
                if (isset($_POST['Cliente'])) {
                        $model->setAttributes($_POST['Cliente']);                     
                        $model->save();
                        if (Yii::app()->request->isAjaxRequest){
                            echo CJSON::encode(array('status'=>'success','div'=>"Cliente creado exitosamente"));
                            exit;               
                        }
                        
                                                
                }               
                if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array('status'=>'failure', 'div'=>$this->renderPartial('_agregar', array('model' => $model,),true)));
                        exit;               
                    }
        }

	public function actionCrear() {
		$model = new Cliente;
                $model->fecha_nacimiento="01/01/1972";
		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Cliente');

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente'])){
			$model->setAttributes($_GET['Cliente']);
                }

                $session['Cliente_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Cliente_model_search']))
               {
                $model = $session['Cliente_model_search'];
                $model = Cliente::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Cliente::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'celular', 'direccion', 'fecha_nacimiento'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Cliente_model_search']))
               {
                $model = $session['Cliente_model_search'];
                $model = Cliente::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Cliente::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'celular', 'direccion', 'fecha_nacimiento'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Cliente'),
		));
	}

	public function actionCreate() {
		$model = new Cliente;

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Cliente');

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);
                        
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('Cliente');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente'])){
			$model->setAttributes($_GET['Cliente']);
                }

                $session['Cliente_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}