<?php

class DetalleProductoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('DetalleProducto');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'DetalleProducto'),
		));
	}

	public function actionCrear() {
		$this->redirect(array('/inventario/ingresarProducto'));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'DetalleProducto');

		$this->performAjaxValidation($model, 'detalle-producto-form');

		if (isset($_POST['DetalleProducto'])) {
			$model->setAttributes($_POST['DetalleProducto']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DetalleProducto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
            $this->redirect(array('/inventario/inventarioDetalle'));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['DetalleProducto_model_search']))
               {
                $model = $session['DetalleProducto_model_search'];
                $model = DetalleProducto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = DetalleProducto::model()->findAll();
             $this->toExcel($model, array('id', 'codigo_barra', 'fecha_creacion', 'estado', 'fecha_modificacion', 'producto', 'compra'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['DetalleProducto_model_search']))
               {
                $model = $session['DetalleProducto_model_search'];
                $model = DetalleProducto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = DetalleProducto::model()->findAll();
             $this->toExcel($model, array('id', 'codigo_barra', 'fecha_creacion', 'estado', 'fecha_modificacion', 'producto', 'compra'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DetalleProducto'),
		));
	}

	public function actionCreate() {
		$model = new DetalleProducto;

		$this->performAjaxValidation($model, 'detalle-producto-form');

		if (isset($_POST['DetalleProducto'])) {
			$model->setAttributes($_POST['DetalleProducto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DetalleProducto');

		$this->performAjaxValidation($model, 'detalle-producto-form');

		if (isset($_POST['DetalleProducto'])) {
			$model->setAttributes($_POST['DetalleProducto']);
                        
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DetalleProducto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('DetalleProducto');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new DetalleProducto('search');
		$model->unsetAttributes();

		if (isset($_GET['DetalleProducto'])){
			$model->setAttributes($_GET['DetalleProducto']);
                }

                $session['DetalleProducto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}