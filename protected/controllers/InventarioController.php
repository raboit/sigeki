<?php
class InventarioController extends GxController{
    
    public function filters() {
                return array('rights');
    }
    
    public function actionInventarioProducto(){
                $this->redirect(array('producto/administrar'));
	}
        
    public function actionGramosServicio(){
                $this->redirect(array('gramosServicio/administrar'));
	}
    
    public function actionInventarioDetalle(){
        $this->redirect(array('producto/administrar'));
	}
    
    public function actionNuevaCompra(){        
        $this->redirect(array('compra/crear'));
    }
    
    public function actionIngresarProducto(){
        Yii::app()->getUser()->setFlash('info','DEBE SELECCIONAR UNA COMPRA (<i class="icon-eye-open"></i>) PARA <strong>INGRESAR PRODUCTOS</strong> ');
        $this->redirect(array('compra/administrar'));
    } 
    public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
    public function actionGenerarExcel()
    {	   
         $session=new CHttpSession;
         $session->open();
         if(isset($session['DetalleProducto_model_search']))
           {
            $model = $session['DetalleProducto_model_search'];
            $model = DetalleProducto::model()->findAll($model->search()->criteria);
           }
           else
             $model = DetalleProducto::model()->findAll();
             $model_detalle = new DetalleProducto;
         $this->toExcel($model, array('producto', 'codigo_barra', 'fecha_creacion', 'estado', 'fecha_modificacion','compra'), $model_detalle->label(2).' '.date('Y-m-d-H-i-s'), array(), 'Excel5');
    }
    public function actionIndex(){
        $this->render('index', array());
    }
}
?>
