<?php

class VentaController extends GxController {

        public function filters() {
                return array('rights');
        }
        public function actionAsignarestilista($id){
            $model = $this->loadModel($id, 'Venta');
            $model_servicio=null;
            
            
            foreach ($model->detalleVentas(array('scopes'=>array('servicios_no_asignados'))) as $detalle_venta) {
                if(isset($detalle_venta))
                    $model_servicio=$detalle_venta->servicio;
                if(isset($_POST['Servicio']) AND isset($model_servicio)){
                    if($_POST['Servicio']['id']!=$model_servicio->id){
                        break;
                    }
                }else{
                    break;
                }
                
            }
            if(isset($_POST['Servicio'])){
                $model_servicio_actualizado= $this->loadModel($_POST['Servicio']['id'], 'Servicio');
                $model_servicio_actualizado->scenario="asignacion";
                $model_servicio_actualizado->estilista_id=($_POST['Servicio']['estilista_id']);
                $model_servicio_actualizado->formula_id=($_POST['Servicio']['formula_id']);
                $model_servicio_actualizado->estado="FINALIZADO";
                
                if($model_servicio_actualizado->save(true)){
                    $model_servicio_actualizado->actualizarStock();
                    Yii::app()->getUser()->setFlash('success','<i class="icon-ok-sign"></i> Estilista asignado exitósamente!');
                }else
                    Yii::app()->getUser()->setFlash('error','Ocurrio un error, intente de nuevo');
                
                if(Yii::app()->request->isAjaxRequest){
                    if($model->verificarServicios()){ 
                        echo CJSON::encode(array('status'=>'success','estado'=>'terminado','mensaje'=>'ASIGNACION DE ESTILISTAS COMPLETADO EXITOSAMENTE','div_venta'=>$this->renderPartial('_venta', array('model' => $model,),true), 'div_detalle'=>$this->renderPartial('_detalles_venta', array('model' => $model),true)));
                        exit;
                    }
                                  
                }
                                       
            }            
            if (Yii::app()->request->isAjaxRequest)
                {
                    if(isset($model_servicio)){
                    echo CJSON::encode(array('status'=>'failure','estado'=>'asignacion simple', 'div'=>$this->renderPartial('asignar_estilista/_formulario', array('model' => $model_servicio,),true)));
                    exit;
                    }else{
                        echo CJSON::encode(array('status'=>'failure','estado'=>'sin servicios', 'div'=>'NO POSEE SERVICIOS PENDIENTES'));
                        exit;
                    }
                }
            
            
            
        }
        public function actionPagarVenta($id){                
            $model = $this->loadModel($id, 'Venta');
            $model->tipo_pago="Efectivo";
            $model->cantidad_pago=$model->total;
            $model->cantidad_vuelto=0;
            
            if(!$model->verificarServicios()){
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array('status'=>'failure', 'div'=>'RECUERDE, UD. DEBE ASIGNAR UN ESTILISTA A LOS SERVICIOS'));
                    exit;               
                }
            }
            if (isset($_POST['Venta'])){
                $model->tipo_pago=($_POST['Venta']['tipo_pago']);
                $model->cantidad_pago=($_POST['Venta']['cantidad_pago']);
                $model->estado="PAGADA";
                $model->actualizarStock('pagar');
                $model->save();
                if(Yii::app()->request->isAjaxRequest){
                    echo CJSON::encode(array('status'=>'success','mensaje'=>'PAGO INGRESADO EXITOSAMENTE', 'div'=>$this->renderPartial('_venta', array('model' => $model,),true)));
                    exit;               
                }                       
            }               
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array('status'=>'failure', 'div'=>$this->renderPartial('_pagar', array('model' => $model,),true)));
                    exit;               
                }
        }
        
	public function actionIndex() {
                $this->render('listar', array());
	}

	public function actionListar() {
            $this->redirect(array('listar'));	
	}        
        
	public function actionAnular($id) {
                $model=$this->loadModel($id, 'Venta');
                $model->estado = "ANULADA";
                $model->actualizarStock('anular');
                $model->validate();
                $model->save();
                $this->redirect(array('ver', 'id' =>$model->id));
	}
        public function actionVer($id) {
                $model=$this->loadModel($id, 'Venta');
                if($model->estado!="NUEVA")
                Yii::app()->getUser()->setFlash('info','<i class="icon-info-sign"></i> Esta venta se encuentra <strong>'.$model->estado.'</strong> ');
		$this->render('ver', array(
			'model' => $model,
		));
	}
        public function actionProcesarProducto(){
            $model_producto=false;
            $model_detalle_producto=null;
            if(isset($_POST['codigo_barra'])){
                $model_detalle_producto = DetalleProducto::model()->findByAttributes(array('estado'=>'NUEVO', 'codigo_barra'=>$_POST['codigo_barra']));
                if(isset($model_detalle_producto)){
                    $model_detalle_producto->estado="PROCESO";
                    $model_detalle_producto->fecha_modificacion=date("Y-m-d H:i:s");
                    $model_detalle_producto->save();
                    Yii::app()->getUser()->setFlash('info','<i class="icon-info-sign"></i> Producto <strong>PROCESADO</strong> exitosamente');
                    $model_producto=true;
                }else{
                    Yii::app()->getUser()->setFlash('error','<i class="icon-info-sign"></i> Producto <strong>NO</strong> encontrado, intente de nuevo');
                }
            }
            $this->render('procesar_producto', array('model_producto'=>$model_producto,'model_detalle_producto'=>$model_detalle_producto));
        }
        
        public function actionGetTotalventa($id){
            $model= $this->loadModel($id, 'Venta');
            echo "$".Yii::app()->format->formatNumber($model->calcularTotal());            
        }
        
        public function actionCrearDetalleVenta($id) {
                $model_detalle_venta = new DetalleVenta;
                $model_venta = $this->loadModel($id,'Venta');
                $model_detalle_venta->venta_id = $model_venta->id;
                $model_producto= new Producto;
                
                if($model_venta->estado=="ANULADA")
                    $this->redirect(array('ver', 'id' => $model_venta->id));  
                                
                if(isset($_POST['no_aplicable'])){
                    $model_detalle_venta->no_aplicable=1;
                }
                if(isset($_POST['agregar'])){
                    if(isset($_POST['codigo_barra'])){
                        $model_tipo_servicio = TipoServicio::model()->findByAttributes(array('codigo_barra'=>$_POST['codigo_barra']));
                        if(isset($model_tipo_servicio)){
                            
                            $model_servicio = new Servicio();
                            $model_servicio->fecha = date("Y-m-d H:i:s");
                            $model_servicio->tipo_servicio_id=$model_tipo_servicio->id;
                            $model_servicio->cliente_id=$model_venta->cliente_id;
                            if($model_detalle_venta->no_aplicable)
                                $model_servicio->estado="N/A";
                            $model_servicio->validate();
                            $model_servicio->save(false);
                            
                            $model_detalle_venta->servicio_id=$model_servicio->id;
                            $model_detalle_venta->precio=$model_tipo_servicio->precio_venta;
                            $model_detalle_venta->validate();
                            $model_detalle_venta->save(false);

                            Yii::app()->getUser()->setFlash('info','<i class="icon-info-sign"></i> Recuerde que debe asignar un estilista al servicio '.$model_tipo_servicio->nombre);
                        }else{
                          $model_producto = Producto::model()->findByAttributes(array('tipo'=>'TERMINADO', 'codigo_barra'=>$_POST['codigo_barra']));
                          if(isset($model_producto)){
                            $model_detalle_venta->producto_id =  $model_producto->id;
                            $model_detalle_venta->precio = $model_producto->precio_venta;
                            if($model_detalle_venta->validate())
                                $model_detalle_venta->save(true);
                            Yii::app()->getUser()->setFlash('success','<i class="icon-ok-sign"></i> Producto agregado exitósamente!');
                        }else{
                            Yii::app()->getUser()->setFlash('error','Debe ingresar un producto disponible en el inventario.');
                        }  
                        }                        
                        
                    }
                }elseif(isset($_POST['terminar'])){                    
                        $model_venta->total=$model_venta->calcularTotal();
                        $model_venta->validate();
                        $model_venta->save(false);
                        $this->redirect(array('ver', 'id' => $model_venta->id));                    
                }
            
		$this->render('crear_detalle_venta', array(
			'model' => $model_venta,
		));
	}

	public function actionCrear() {
		$model = new Venta;
                $model->fecha = date("Y-m-d H:i:s");
                $turno_actual=Turno::getTurnoActual();
                $model->turno_id= $turno_actual['turno_id'];
                $model->cajero_id=$turno_actual['cajero_id']; 
                
		$this->performAjaxValidation($model, 'venta-form');

		if (isset($_POST['Venta'])) {
			$model->setAttributes($_POST['Venta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('crearDetalleVenta', 'id' => $model->id));
			}
		}
		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Venta');

		$this->performAjaxValidation($model, 'venta-form');

		if (isset($_POST['Venta'])) {
			$model->setAttributes($_POST['Venta']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Venta')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Venta('search');
		$model->unsetAttributes();

		if (isset($_GET['Venta'])){
			$model->setAttributes($_GET['Venta']);
                }

                $session['Venta_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Venta_model_search']))
               {
                $model = $session['Venta_model_search'];
                $model = Venta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Venta::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'total', 'cajero', 'turno', 'cliente'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Venta_model_search']))
               {
                $model = $session['Venta_model_search'];
                $model = Venta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Venta::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'total', 'cajero', 'turno', 'cliente'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}
