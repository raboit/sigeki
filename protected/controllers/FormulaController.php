<?php

class FormulaController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Formula');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Formula'),
		));
	}

	public function actionCrear() {
		$model = new Formula;
                $model->fecha_creacion = date("Y-m-d H:i:s",time());
		$this->performAjaxValidation($model, 'formula-form');

		if (isset($_POST['Formula'])) {
			$model->setAttributes($_POST['Formula']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Formula');
                $model->fecha_edicion = date("Y-m-d H:i:s",time());
		$this->performAjaxValidation($model, 'formula-form');

		if (isset($_POST['Formula'])) {
			$model->setAttributes($_POST['Formula']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Formula')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Formula('search');
		$model->unsetAttributes();

		if (isset($_GET['Formula'])){
			$model->setAttributes($_GET['Formula']);
                }

                $session['Formula_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Formula_model_search']))
               {
                $model = $session['Formula_model_search'];
                $model = Formula::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Formula::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'fecha_creacion', 'fecha_edicion', 'cliente'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Formula_model_search']))
               {
                $model = $session['Formula_model_search'];
                $model = Formula::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Formula::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'fecha_creacion', 'fecha_edicion', 'cliente'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Formula'),
		));
	}

	public function actionCreate() {
		$model = new Formula;

		$this->performAjaxValidation($model, 'formula-form');

		if (isset($_POST['Formula'])) {
			$model->setAttributes($_POST['Formula']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Formula');

		$this->performAjaxValidation($model, 'formula-form');

		if (isset($_POST['Formula'])) {
			$model->setAttributes($_POST['Formula']);
                        
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Formula')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('Formula');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Formula('search');
		$model->unsetAttributes();

		if (isset($_GET['Formula'])){
			$model->setAttributes($_GET['Formula']);
                }

                $session['Formula_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}