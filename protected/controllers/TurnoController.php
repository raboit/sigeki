<?php

class TurnoController extends GxController {

    public function filters() {
            return array('rights');
    }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$this->redirect(array('administrar'));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Turno'),
		));
	}
        /******************************
         * Caso de uso : Crear Turno
         * Objetivo: crear un turno par aun cajero especifico.
         * Este caso de uso se da por terminado cuando se crea exitosament un turno y se cierra el anterior.
         * 
         * Restricciones:
         * -Solo debe existir un turno activo.
         * -El turno debe tener asociado un cajero.
         *******************************/
	public function actionCrear() {
		$model_turno = new Turno;
                $model_turno->fecha_inicio=date('Y-m-d H:i:s');
                $this->performAjaxValidation($model_turno, 'turno-form');
		if (isset($_POST['Turno'])){
                    $model_turno->setAttributes($_POST['Turno']);                        
                    $model_turno->estado="ACTIVO";
                    if($model_turno->validate()){
                        $this->cerrarturnoanterior();
                        if($model_turno->save()){
                        if (Yii::app()->getRequest()->getIsAjaxRequest())
                                Yii::app()->end();
                        else
                            $this->redirect(array('ver', 'id' => $model_turno->id));
                        }    
                    }
                                       
		}
		$this->render('crear', array( 'model' => $model_turno));		
	}
        
        private function cerrarturnoanterior(){

            $model_turno_anterior = Turno::model()->findByAttributes(array('estado'=>"ACTIVO"),array('order'=>'id DESC'))?:null;
            if(isset($model_turno_anterior)){
                $model_turno_anterior->estado="CERRADO";
                $model_turno_anterior->fecha_termino=date('Y-m-d H:i:s');
                $model_turno_anterior->save(true);
            }
            
        }
        
	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Turno');
		$this->performAjaxValidation($model, 'turno-form');

		if (isset($_POST['Turno'])) {
			$model->setAttributes($_POST['Turno']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Turno')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
            
            Yii::app()->getUser()->setFlash('info','DEBE SELECCIONAR UN TURNO (<i class="icon-eye-open"></i>) ');
                $session = new CHttpSession;
                $session->open();
		$model = new Turno('search');
		$model->unsetAttributes();

		if (isset($_GET['Turno'])){
			$model->setAttributes($_GET['Turno']);
                }

                $session['Turno_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Turno_model_search']))
               {
                $model = $session['Turno_model_search'];
                $model = Turno::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Turno::model()->findAll();
             $this->toExcel($model, array('id', 'fecha_inicio', 'fecha_termino', 'cajero'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Turno_model_search']))
               {
                $model = $session['Turno_model_search'];
                $model = Turno::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Turno::model()->findAll();
             $this->toExcel($model, array('id', 'fecha_inicio', 'fecha_termino', 'cajero'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}