<?php

class CompraController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Compra');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
            $model_compra = $this->loadModel($id,'Compra');
            //$model_detalle_producto= new DetalleProducto;
            
            if(isset($_POST['codigo_barra'])):
                $model_detalle_producto->codigo_barra=$_POST['codigo_barra'];
                $model_detalle_producto->compra_id=$id;
                $model_detalle_producto->producto_id=$model_compra->producto_id;
                $model_detalle_producto->fecha_creacion=date("Y-m-d H:i:s");
                $model_detalle_producto->estado="NUEVO";
                if($model_detalle_producto->validate()){
                    $model_detalle_producto->save(true);
                    Yii::app()->getUser()->setFlash('success','PRODUCTO INGRESADO EXITASAMENTE ');
                           
                }else{
                    Yii::app()->getUser()->setFlash('error','EL PRODUCTO YA SE ENCUENTRA REGISTRADO ');
                }
                
            endif;
            
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Compra'),
		));
	}

	public function actionCrear() {
            $model = new Compra;
                $model->fecha_compra = date("Y-m-d");

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);

			if ($model->save()) {
                            #$model->producto->actualizarStock();
                            Yii::app()->getUser()->setFlash('success','<i class="icon-ok-sign"></i> COMPRA INGRESADA EXITASAMENTE ');
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('/producto/ver', 'id' => $model->producto->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
        }

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Compra');

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Compra')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Compra('search');
		$model->unsetAttributes();

		if (isset($_GET['Compra'])){
			$model->setAttributes($_GET['Compra']);
                }

                $session['Compra_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Compra_model_search']))
               {
                $model = $session['Compra_model_search'];
                $model = Compra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Compra::model()->findAll();
             $this->toExcel($model, array('id', 'precio_compra', 'fecha_compra', 'producto', 'proveedor'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Compra_model_search']))
               {
                $model = $session['Compra_model_search'];
                $model = Compra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Compra::model()->findAll();
             $this->toExcel($model, array('id', 'precio_compra', 'fecha_compra', 'producto', 'proveedor'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Compra'),
		));
	}

	public function actionCreate() {
		$model = new Compra;

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Compra');

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);
                        
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Compra')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('Compra');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Compra('search');
		$model->unsetAttributes();

		if (isset($_GET['Compra'])){
			$model->setAttributes($_GET['Compra']);
                }

                $session['Compra_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}
