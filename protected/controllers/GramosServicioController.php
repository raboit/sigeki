<?php

class GramosServicioController extends GxController {

        public function filters() {
            return array('rights');
        }
	

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new GramosServicio('search');
		$model->unsetAttributes();

		if (isset($_GET['GramosServicio'])){
			$model->setAttributes($_GET['GramosServicio']);
                }

                $session['GramosServicio_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['GramosServicio_model_search']))
               {
                $model = $session['GramosServicio_model_search'];
                $model = GramosServicio::model()->findAll($model->search()->criteria);
               }
               else
                 $model = GramosServicio::model()->findAll();
             $this->toExcel($model, array('nombre_tipo_servicio', 'fecha_servicio', 'nombre_estilista', 'nombre_producto', 'cantidad_gr'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['GramosServicio_model_search']))
               {
                $model = $session['GramosServicio_model_search'];
                $model = GramosServicio::model()->findAll($model->search()->criteria);
               }
               else
                 $model = GramosServicio::model()->findAll();
             $this->toExcel($model, array('nombre_tipo_servicio', 'fecha_servicio', 'nombre_estilista', 'nombre_producto', 'cantidad_gr'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	
}