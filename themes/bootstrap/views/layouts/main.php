<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
	<meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" /> 
        <?php Yii::app()->bootstrap->register(); ?>
        <?php #Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/metro-bootstrap.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/styles.css'); ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
    
    <?php
//Render Menu
//$rol = isset(Yii::app()->user->rol)? Yii::app()->user->rol : null; // ***Uso con cautela*** Almacenado en una cookie
    $rol = !Yii::app()->user->isGuest?User::model()->findByAttributes(array('id'=>Yii::app()->user->id))->rol:null;

if($rol === null) $this->renderPartial('//layouts/menus/_invitado');
else
switch ($rol){

        case "Administrador":
                $this->renderPartial('//layouts/menus/_admin');
                break;
        case "Cajero":
                $this->renderPartial('//layouts/menus/_cajero');
                break;
        case "Estilista":
                $this->renderPartial('//layouts/menus/_estilista');
                break;
        default:
                $this->renderPartial('//layouts/menus/_usuario');                
}
?>
       
<div class="container" id="page">
        
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
                'links'=>$this->breadcrumbs,
        )); ?>

	<?php echo $content; ?>

	<div class="clear"></div>
        
        <hr>

	<div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> por Kika's Salon<br/>
		Todos los derechos reservados.<br/>
		Desarrollado por <a href="http://www.raboit.com/" rel="external">Rabo IT</a>.
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
