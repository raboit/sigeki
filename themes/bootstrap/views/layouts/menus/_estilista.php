<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'color'=>  TbHtml::NAVBAR_COLOR_INVERSE,
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'items'=>array(
                    array('label'=>'Ver Formula', 'url'=>array('/servicio/verFormula')),
                
                /*  array('label'=>'Ventas', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Nueva Venta', 'url'=>array('/venta/crear')),
                    array('label'=>'Procesar producto', 'url'=>array('/venta/procesarProducto')),
                    array('label'=>'Admin Ventas', 'url'=>array('/venta/administrar')),
                )),
                
                array('label'=>'Servicios', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Admin servicios', 'url'=>array('/servicio/administrar')),
                    
                )),
                
                array('label'=>'Inventario', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Inventario de productos', 'url'=>array('/inventario/inventarioProducto')),
                    array('label'=>'Inventario al detalle', 'url'=>array('/inventario/inventarioDetalle')),
                    array('label'=>'Nueva compra', 'url'=>array('/inventario/nuevaCompra')),
                    array('label'=>'Registrar productos', 'url'=>array('/inventario/ingresarProducto')),
                )),
                
                array('label'=>'Funcionario', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Estilista', 'url'=>array('/estilista/administrar')),
                    array('label'=>'Cajero', 'url'=>array('/cajero/administrar')),
                )),
                
                array('label'=>'Mantenedores', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Clientes', 'url'=>array('/cliente/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Formulas', 'url'=>array('/formula/administrar')),                    
                    TbHtml::menuDivider(),
                    array('label'=>'Tipos de Servicios', 'url'=>array('/tipoServicio/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Productos', 'url'=>array('/producto/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Compras', 'url'=>array('/compra/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Proveedores', 'url'=>array('/proveedor/administrar')),
                )),
                
                                
                array('label'=>'Usuarios', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Usuarios', 'url'=>array('/user/index')),
                    array('label'=>'Roles', 'url'=>array('/rights/')),
                    TbHtml::menuDivider(),
                    array('label'=>'Cambiar Contraseña', 'url'=>array('/user/changePassword/', "id"=>Yii::app()->user->id)),
                )),*/
            ),
        ),
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Salir ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>