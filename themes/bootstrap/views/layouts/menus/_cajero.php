<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'color'=>  TbHtml::NAVBAR_COLOR_INVERSE,
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'items'=>array(                
                    array('label'=>'Nueva Venta', 'url'=>array('/venta/crear')),
                    array('label'=>'Administrar Ventas', 'url'=>array('/venta/administrar')),
                
                
                //array('label'=>'Procesar Producto', 'url'=>array('/venta/procesarProducto')),
                
                
                /*    
                array('label'=>'Evaluaciones', 'url'=>'#', 'items'=>array(
                    array('label'=>'Evaluación', 'url'=>array('/evaluacion/list')),
                    array('label'=>'Evaluación Semestral', 'url'=>array('/evaluacionSemestral/list')),
                )),                    
                
                array('label'=>'Calendario Docente', 'url'=>array('/calendarioDocente/list')),    
                    
                array('label'=>'Planes', 'url'=>'#', 'items'=>array(
                    array('label'=>'Plan Actividad', 'url'=>array('/planActividad/list')),
                    array('label'=>'Planinifcación Semestral', 'url'=>array('/planificacionSemestral/list')),
                )),
                    
                array('label'=>'Personas', 'url'=>'#', 'items'=>array(
                    array('label'=>'Alumno', 'url'=>array('/alumno/list')),
                    array('label'=>'Profesor', 'url'=>array('/profesor/list')),
                )),*/
            ),
        ),
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=> Yii::app()->user->name, 'items'=>array(
                    array('label'=>'Salir', 'url'=>array('/site/logout')),
                    TbHtml::menuDivider(),
                    array('label'=>'Cambiar Contraseña', 'url'=>array('/user/changePassword/', "id"=>Yii::app()->user->id)),
                )),
            ),
        ),
    ),
)); ?>