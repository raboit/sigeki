<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
//$this->breadcrumbs=array(
//        Yii::t('app', 'Index'),
//);
?>

<?php $this->widget('bootstrap.widgets.TbHeroUnit', array(
    'heading' => 'Bienvenido a '.CHtml::encode(Yii::app()->name),
    'content' => '<p>Bienvenido a SIGEKI</p>',
)); ?>
<div class="container-fluid">
<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('inventario/');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Módulo Inventario" ?></h4>
                        <?php echo "Ver stock disponible de productos"; ?> 

                        </span>
                </div>
        </a>
</div>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('venta/');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Módulo Venta" ?></h4>

                        <?php echo "Ver unidades de productos" ?>
                         
                        </span>
                </div>
        </a>
</div>
<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('funcionario/');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Módulo Funcionario" ?></h4>
                        <?php echo "Ver funcionarios del sistema"; ?>
                        </span>
                </div>
        </a>
</div>

<div class="span3">
        <a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('servicio/');?>">
                <div class="view well cuadro">
                        <span>
                        <h4><?php echo "Módulo Servicio" ?></h4>
                        <?php echo "Ingresar unidades de productos"; ?>
                        </span>
                </div>
        </a>
</div>

</div>
